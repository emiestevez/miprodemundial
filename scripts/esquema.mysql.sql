drop table if exists grupo;
drop table if exists equipo;
drop table if exists usuario;
drop table if exists rol;
drop table if exists partido;
drop table if exists pronostico;
drop table if exists mundial;
drop table if exists prode;
drop table if exists torneo;
drop trigger if exists prode_pronostico;


create table usuario (
    id bigint not null auto_increment primary key,
    nombre varchar(255) not null,
    email varchar(255) not null unique,
    password varchar(255) null,
    enabled boolean  null,
    inscripcion TIMESTAMP DEFAULT NOW()
);

create table rol (
    id bigint not null auto_increment primary key,
    id_usuario bigint not null,
    permiso varchar(255) not null
);

create table mundial (
    id bigint not null auto_increment primary key,
    nombre varchar(20) not null
);

create table grupo (
    id bigint not null auto_increment primary key,
    nombre varchar(10) not null,
    id_mundial bigint
);

create table equipo (
    id bigint not null auto_increment primary key,
    nombre varchar(20) not null,
    bandera varchar(50) not null,
    nombre_grupo varchar(20) not null,
    id_grupo bigint not null
);

create table partido (
    id bigint not null auto_increment primary key,
    fecha TIMESTAMP DEFAULT NOW(),
    equipo_local_id bigint,
    equipo_visitante_id bigint,
    goles_local int,
    goles_visitante int,
    partido_terminado boolean default false
);

create table torneo (
    id bigint not null auto_increment primary key,
    nombre varchar(50) not null,
    id_usuario bigint not null
);

create table prode (
    id bigint not null auto_increment primary key,
    puntos int,
    id_usuario bigint ,
    id_torneo bigint
);

create table pronostico (
    id bigint not null auto_increment primary key,
    id_partido bigint,
    id_prode bigint,
    id_grupo bigint,
    goles_local int,
    goles_visitante int
);

-- triggers
delimiter //
CREATE TRIGGER prode_pronostico AFTER INSERT ON prode
    FOR EACH ROW
    BEGIN
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 1, 1, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 2, 1, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 3, 2, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 4, 2, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 5, 3, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 6, 3, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 7, 4, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 8, 4, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 9, 5, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 10, 5, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 11, 6, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 12, 6, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 13, 7, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 14, 7, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 15, 8, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 16, 8, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 17, 1, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 18, 1, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 19, 2, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 20, 2, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 21, 3, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 22, 3, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 23, 4, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 24, 4, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 25, 5, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 26, 5, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 27, 6, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 28, 6, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 29, 7, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 30, 7, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 31, 8, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 32, 8, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 33, 1, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 34, 1, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 35, 2, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 36, 2, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 37, 3, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 38, 3, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 39, 4, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 40, 4, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 41, 5, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 42, 5, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 43, 6, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 44, 6, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 45, 7, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 46, 7, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 47, 8, 0, 0);
        INSERT INTO pronostico (id_prode, id_partido, id_grupo, goles_local, goles_visitante) values (new.id, 48, 8, 0, 0);
     END;//
delimiter ;

-- ---------------------------------------------------------------------------
-- Insert de datos
-- ---------------------------------------------------------------------------
-- Usuario
insert into usuario (id, nombre, email, password, enabled) values (1, 'usuario1', 'usuario1@prodemundial.com', 'ce3a6c2019eeecaf36145479bf2a5e308d9e471da80e2dcec1b60656066eec231ab5b9f76e55ebbd', true);

insert into rol(id, id_usuario, permiso) values (1, 1, 'admin');

-- torneo
insert into torneo(id, nombre, id_usuario) values (1, 'mi mundial', 1);

-- Prode
insert into prode(id, puntos, id_usuario, id_torneo) values (1, 0, 1, 1);

-- Mundial
insert into mundial(id, nombre) values (1, 'Brasil 2014');

-- Grupo
insert into grupo(id, nombre, id_mundial) values (1, 'Grupo A', 1);
insert into grupo(id, nombre, id_mundial) values (2, 'Grupo B', 1);
insert into grupo(id, nombre, id_mundial) values (3, 'Grupo C', 1);
insert into grupo(id, nombre, id_mundial) values (4, 'Grupo D', 1);
insert into grupo(id, nombre, id_mundial) values (5, 'Grupo E', 1);
insert into grupo(id, nombre, id_mundial) values (6, 'Grupo F', 1);
insert into grupo(id, nombre, id_mundial) values (7, 'Grupo G', 1);
insert into grupo(id, nombre, id_mundial) values (8, 'Grupo H', 1);

-- Equipo
-- Grupo A
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (1, 'BRZ','img/paises/brasil.png', 'Brazil', 1);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (2, 'CRO','img/paises/croacia.png', 'Croacia', 1);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (3, 'MEX','img/paises/mexico.png', 'Mexico', 1);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (4, 'CAM','img/paises/camerun.png', 'Camerun', 1);

-- Grupo B
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (5, 'ESP','img/paises/espana.png', 'Espa�a', 2);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (6, 'NED','img/paises/holanda.png', 'Holanda', 2);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (7, 'CHI','img/paises/chile.png', 'Chile', 2);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (8, 'AUS','img/paises/australia.png', 'Australia', 2);

-- Grupo C
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (9, 'COL','img/paises/colombia.png', 'Colombia', 3);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (10, 'GRE','img/paises/grecia.png', 'Gracia', 3);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (11, 'CIV','img/paises/costaDeMarfil.png', 'Costa de Marfil', 3);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (12, 'JPN','img/paises/japon.png', 'Japon', 3);

-- Grupo D
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (13, 'URU','img/paises/uruguay.png', 'Uruguay', 4);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (14, 'CRC','img/paises/costa_rica.png', 'Costa Rica', 4);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (15, 'ENG','img/paises/inglaterra.png', 'Inglaterra', 4);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (16, 'ITA','img/paises/italia.png', 'Italia', 4);

-- Grupo E
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (17, 'SUI','img/paises/suiza.png', 'Suiza', 5);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (18, 'ECU','img/paises/ecuador.png', 'Ecuador', 5);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (19, 'FRA','img/paises/francia.png', 'Francia', 5);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (20, 'HON','img/paises/honduras.png', 'Honduras', 5);

-- Grupo F
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (21, 'ARG','img/paises/argentina.png', 'Argentina', 6);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (22, 'BIH','img/paises/bosnia.png', 'Bosnia y Herzegovina', 6);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (23, 'IRN','img/paises/iran.png', 'Iran', 6);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (24, 'NGA','img/paises/nigeria.png', 'Nigeria', 6);

-- Grupo G
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (25, 'GER','img/paises/alemania.png', 'Alemania', 7);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (26, 'POR','img/paises/portugal.png', 'Portugal', 7);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (27, 'GHA','img/paises/ghana.png', 'Ghana', 7);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (28, 'USA','img/paises/eeuu.png', 'Usa', 7);

-- Grupo H
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (29, 'BEL','img/paises/belgica.png', 'Belgica', 8);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (30, 'ALG','img/paises/argelia.png', 'Argelia', 8);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (31, 'RUS','img/paises/rusia.png', 'Rusia', 8);
insert into equipo (id, nombre,  bandera, nombre_grupo, id_grupo) values (32, 'KOR','img/paises/corea_del_sur.png', 'Corea del sur', 8);

-- Partido
-- Grupo A
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (1, '2014-06-12', 1, 2, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (2, '2014-06-13', 3, 4, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (17, '2014-06-17', 1, 3, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (18, '2014-06-18', 4, 2, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (33, '2014-06-23', 4, 1, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (34, '2014-06-23', 2, 3, 0, 0);

-- Grupo B
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (3, '2014-06-13', 5, 6, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (4, '2014-06-13', 7, 8, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (19, '2014-06-18', 5, 7, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (20, '2014-06-18', 8, 6, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (35, '2014-06-23', 8, 5, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (36, '2014-06-23', 6, 7, 0, 0);

-- Grupo C
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (5, '2014-06-14', 9, 10, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (6, '2014-06-14', 11, 12, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (21, '2014-06-19', 9, 11, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (22, '2014-06-19', 12, 10, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (37, '2014-06-24', 12, 9, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (38, '2014-06-24', 10, 11, 0, 0);

-- Grupo D
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (7, '2014-06-14', 13, 14, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (8, '2014-06-14', 15, 16, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (23, '2014-06-19', 13, 15, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (24, '2014-06-19', 16, 14, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (39, '2014-06-24', 16, 13, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (40, '2014-06-24', 14, 15, 0, 0);

-- Grupo E
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (9, '2014-06-15', 17, 18, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (10, '2014-06-15', 19, 20, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (25, '2014-06-20', 17, 19, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (26, '2014-06-20', 20, 18, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (41, '2014-06-25', 20, 17, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (42, '2014-06-25', 18, 19, 0, 0);

-- Grupo F
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (11, '2014-06-15', 21, 22, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (12, '2014-06-16', 23, 24, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (27, '2014-06-21', 21, 23, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (28, '2014-06-21', 24, 22, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (43, '2014-06-25', 24, 21, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (44, '2014-06-25', 22, 23, 0, 0);

-- Grupo G
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (13, '2014-06-16', 25, 26, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (14, '2014-06-16', 27, 28, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (29, '2014-06-21', 25, 27, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (30, '2014-06-22', 28, 26, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (45, '2014-06-26', 28, 25, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (46, '2014-06-26', 26, 27, 0, 0);

-- Grupo H
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (15, '2014-06-17', 29, 30, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (16, '2014-06-17', 31, 32, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (31, '2014-06-22', 29, 31, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (32, '2014-06-22', 32, 30, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (47, '2014-06-26', 32, 29, 0, 0);
insert into partido (id, fecha, equipo_local_id, equipo_visitante_id, goles_local, goles_visitante) values (48, '2014-06-26', 30, 31, 0, 0);
