/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.controller;

import com.prode.mundial.domain.Torneo;
import com.prode.mundial.vo.IntegranteVo;
import com.prode.mundial.vo.TorneoVo;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 *
 * @author eestevez
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:mvc.xml", "classpath:config.xml", "classpath:repository.xml", "classpath:db-test.xml", "classpath:security.xml"})
public class TorneoControllerTest {

    @Autowired
    private TorneoController torneoController;

    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    private DataSource dataSource;
    
    private JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void buscarRanking_conTorneoYParticipanteValido_retornaTorneo() {
        login();
        Torneo torneo = torneoController.buscarRanking();
        Assert.assertNotNull(torneo);
        Assert.assertTrue(torneo.getProdes().size() > 0);
    }

    @Test
    public void crear_conUsuarioLogueadoYLosDatosDeLosIntegrantesCorrectors_creaTorneo() {
        login();
        
        TorneoVo torneoVo = new TorneoVo();
        torneoVo.setNombre("el mundial de messi");
        IntegranteVo integrante = new IntegranteVo();
        integrante.setNombre("pepe");
        integrante.setEmail("pepe@mail.com");
        List<IntegranteVo> integrantes = new ArrayList<IntegranteVo>();
        integrantes.add(integrante);
        integrante = new IntegranteVo();
        integrante.setNombre("juan");
        integrante.setEmail("juna@mail.com");
        integrantes.add(integrante);
        torneoVo.setIntegrantes(integrantes);
        
        int cantidadUsuarioInicial = JdbcTestUtils.countRowsInTable(jdbcTemplate, "usuario");
        int cantidadTorneoInicial = JdbcTestUtils.countRowsInTable(jdbcTemplate, "torneo");
        int cantidadPronosticosInicial = JdbcTestUtils.countRowsInTable(jdbcTemplate, "pronostico");
        int cantidadProdeInicial = JdbcTestUtils.countRowsInTable(jdbcTemplate, "prode");
        
        torneoController.crear(torneoVo);
        
        int cantidadUsuarioFinal = JdbcTestUtils.countRowsInTable(jdbcTemplate, "usuario");
        int cantidadTorneoFinal = JdbcTestUtils.countRowsInTable(jdbcTemplate, "torneo");
        int cantidadPronosticosFinal = JdbcTestUtils.countRowsInTable(jdbcTemplate, "pronostico");
        int cantidadProdeFinal = JdbcTestUtils.countRowsInTable(jdbcTemplate, "prode");
        
        Assert.assertEquals(cantidadUsuarioInicial + 2, cantidadUsuarioFinal);
        Assert.assertEquals(cantidadTorneoInicial + 1, cantidadTorneoFinal);
        Assert.assertEquals(cantidadProdeInicial + 2, cantidadProdeFinal);
        Assert.assertEquals(cantidadPronosticosInicial + 8, cantidadPronosticosFinal);
    }
    
    @Test
    public void buscar_conUsuarioLogueadoYTorneoCreado_retornaTorneo() {
        login();
        Torneo torneo = torneoController.buscar();
        Assert.assertNotNull(torneo);
        Assert.assertEquals("mi mundial", torneo.getNombre());
    }
    
    @Test(expected = DataRetrievalFailureException.class)
    public void buscar_conUsuarioLogueadoYTorneoNoCreado_retornaExcepcion() {
        loginConUsuarioSinTorneo();
        torneoController.buscar();
    }

    private void login() throws AuthenticationException {
        Authentication request = new UsernamePasswordAuthenticationToken("usuario1@prodemundial.com", "123456");
        Authentication result = authenticationManager.authenticate(request);
        SecurityContextHolder.getContext().setAuthentication(result);
    }
    
    private void loginConUsuarioSinTorneo() throws AuthenticationException {
        Authentication request = new UsernamePasswordAuthenticationToken("usuario3@prodemundial.com", "123456");
        Authentication result = authenticationManager.authenticate(request);
        SecurityContextHolder.getContext().setAuthentication(result);
    }
}
