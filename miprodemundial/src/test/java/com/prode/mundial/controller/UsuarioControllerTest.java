/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.controller;

import com.prode.mundial.domain.Usuario;
import com.prode.mundial.repository.UsuarioRepository;
import com.prode.mundial.vo.UsuarioVo;
import javax.sql.DataSource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

/**
 *
 * @author Emiliano
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:mvc.xml", "classpath:config.xml", "classpath:repository.xml", "classpath:db-test.xml", "classpath:security.xml", "classpath:email.xml"})
public class UsuarioControllerTest {

    @Autowired
    private UsuarioController usuarioController;
    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    private UsuarioRepository usuarioRepository;

    @Before
    public void setUp() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void guardar_conDatosDelUsuarioCorrectos_retornaUsuarioCreado() {
        int cantidadUsuarioAntes = JdbcTestUtils.countRowsInTable(jdbcTemplate, "USUARIO");
        int cantidadInicialProde = JdbcTestUtils.countRowsInTable(jdbcTemplate, "prode");
        int cantidadInicialGrupo = JdbcTestUtils.countRowsInTable(jdbcTemplate, "grupo");
        int cantidadInicialEquipo = JdbcTestUtils.countRowsInTable(jdbcTemplate, "equipo");
        int cantidadInicialPartido = JdbcTestUtils.countRowsInTable(jdbcTemplate, "partido");
        int cantidadInicialPronostico = JdbcTestUtils.countRowsInTable(jdbcTemplate, "pronostico");

        UsuarioVo usuario = new UsuarioVo();
        usuario.setNombre("Bart");
        usuario.setEmail("bart@gmail.com");
        usuario.setPassword("1231313");
        usuario.setNombreTorneo("the simpsons");
        usuarioController.guardar(usuario);

        int cantidadUsuarioDespues = JdbcTestUtils.countRowsInTable(jdbcTemplate, "USUARIO");
        int cantidadDespuesProde = JdbcTestUtils.countRowsInTable(jdbcTemplate, "prode");
        int cantidadDespuesGrupo = JdbcTestUtils.countRowsInTable(jdbcTemplate, "grupo");
        int cantidadDespuesEquipo = JdbcTestUtils.countRowsInTable(jdbcTemplate, "equipo");
        int cantidadDespuesPartido = JdbcTestUtils.countRowsInTable(jdbcTemplate, "partido");
        int cantidadDespuesPronostico = JdbcTestUtils.countRowsInTable(jdbcTemplate, "pronostico");
        
        Assert.assertEquals(cantidadUsuarioAntes + 1, cantidadUsuarioDespues);
        Assert.assertEquals(cantidadInicialProde + 1, cantidadDespuesProde);
        Assert.assertEquals(cantidadInicialGrupo, cantidadDespuesGrupo);
        Assert.assertEquals(cantidadInicialEquipo, cantidadDespuesEquipo);
        Assert.assertEquals(cantidadInicialPartido, cantidadDespuesPartido);
        Assert.assertEquals(cantidadInicialPronostico + 4, cantidadDespuesPronostico);
    }
    
   @Test
   public void buscar_conUsuarioLogueado_retornaDatosDelUsuario(){
       login();
       Usuario usuario = usuarioController.buscar();
       Assert.assertEquals("usuario1", usuario.getNombre());
       Assert.assertEquals("usuario1@prodemundial.com", usuario.getEmail());
   }
   
   @Test
   public void recuperarContraseña_conEmailValido_enviaMailAlUsuario() {
       usuarioController.recuperar("usuario1@prodemundial.com");
   }
   
   @Test(expected = DataRetrievalFailureException.class)
   public void recuperarContraseña_conEmailInvalido_retornaExcepcion() {
       usuarioController.recuperar("us1@prodemundial.com");
   }
   
   private void login() throws AuthenticationException {
        Authentication request = new UsernamePasswordAuthenticationToken("usuario1@prodemundial.com", "123456");
        Authentication result = authenticationManager.authenticate(request);
        SecurityContextHolder.getContext().setAuthentication(result);
    }
    
}