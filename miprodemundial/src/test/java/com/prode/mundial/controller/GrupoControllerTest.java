/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.controller;

import com.prode.mundial.domain.Grupo;
import java.util.List;
import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Emiliano
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:mvc.xml", "classpath:config.xml", "classpath:repository.xml", "classpath:db-test.xml", "classpath:security.xml"})
public class GrupoControllerTest {

    @Autowired
    private GrupoController instance;
    
    @Test
    public void buscar_conGruposExistentes_retornaListaDeGrupos() {
        List<Grupo> listaDeGrupos = instance.buscar();
        
        Assert.assertNotNull(listaDeGrupos);
        Assert.assertTrue(listaDeGrupos.size()>0);
    }
}
