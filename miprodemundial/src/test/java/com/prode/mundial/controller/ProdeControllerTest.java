/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.controller;

import com.prode.mundial.domain.Prode;
import com.prode.mundial.domain.Pronostico;
import com.prode.mundial.vo.PronosticoVo;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author eestevez
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:mvc.xml", "classpath:config.xml", "classpath:repository.xml", "classpath:db-test.xml", "classpath:security.xml"})
public class ProdeControllerTest {

    @Autowired
    private ProdeController mundialController;

    @Autowired
    private AuthenticationManager authenticationManager;
    
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    private DataSource dataSource;
    
    @Before
    public void setUp(){
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Test
    public void buscar_conUsuarioLogueado_retornaProdeDelUsuario() {
        login("usuario1@prodemundial.com", "123456");
        Prode prode = mundialController.buscar();
        Assert.assertNotNull(prode.getMundial());
        Assert.assertEquals(4, prode.getPronostico().size());
    }
    
    @Test
    public void actualizarPronosticos_conUsuarioLogueadoYListaDePronosticos_retornaProdeDelUsuario() {
        List<Pronostico> pronosticos = new ArrayList<Pronostico>();
        Pronostico pronostico = new Pronostico();
        pronostico.setId(1l);
        pronostico.setGolesLocal(1);
        pronostico.setGolesVisitante(2);
        pronosticos.add(pronostico);
        
        int golesLocalInicial = jdbcTemplate.queryForInt("select goles_local from pronostico where id = 1");
        int golesVisitanteInicial = jdbcTemplate.queryForInt("select goles_visitante from pronostico where id = 1");
        
        login("usuario1@prodemundial.com", "123456");
        mundialController.actualizarPronosticos(pronosticos);
        
        int golesLocalFinal = jdbcTemplate.queryForInt("select goles_local from pronostico where id = 1");
        int golesVisitanteFinal = jdbcTemplate.queryForInt("select goles_visitante from pronostico where id = 1");
        
        Assert.assertNotEquals(golesLocalInicial, golesLocalFinal);
        Assert.assertNotEquals(golesVisitanteInicial, golesVisitanteFinal);
        
        Assert.assertEquals(1, golesLocalFinal);
        Assert.assertEquals(2, golesVisitanteFinal);
    }
    
    @Test(expected = AccessDeniedException.class)
    public void actualizarPronosticos_conUsuarioLogueadoYListaDePronosticosDeOtroUsuario_retornaExcepcion() {
        List<Pronostico> pronosticos = new ArrayList<Pronostico>();
        Pronostico pronostico = new Pronostico();
        pronostico.setId(1l);
        pronostico.setGolesLocal(1);
        pronostico.setGolesVisitante(2);
        pronosticos.add(pronostico);
        
        login("usuario2@prodemundial.com", "123456");
        mundialController.actualizarPronosticos(pronosticos);
    }
    
    private void login(String usuario, String password) throws AuthenticationException {
        Authentication request = new UsernamePasswordAuthenticationToken(usuario, password);
        Authentication result = authenticationManager.authenticate(request);
        SecurityContextHolder.getContext().setAuthentication(result);
    }
}
