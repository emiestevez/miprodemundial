package com.prode.mundial.controller;

import com.prode.mundial.domain.Partido;
import com.prode.mundial.domain.Prode;
import com.prode.mundial.vo.PartidoVo;
import java.util.List;
import javax.sql.DataSource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author eestevez
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:mvc.xml", "classpath:config.xml", "classpath:repository.xml", "classpath:db-test.xml", "classpath:security.xml", "classpath:email.xml"})
public class MundialControllerTest {

    @Autowired
    private MundialController mundialController;
    
    @Autowired
    private AuthenticationManager authenticationManager;
    
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    private DataSource dataSource;

    @Before
    public void setUp(){
        jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    @Test
    public void buscarProximosPartidos_conUsuarioLogueado_retornaListaDePartidosAJugar() {
        login();
        List<Partido> listaDePartidos = mundialController.buscarProximosPartidos();
        Assert.assertNotNull(listaDePartidos);
        Assert.assertEquals(3, listaDePartidos.size());
    }
    
    @Test
    public void actualizar_conUsuarioConRolAdmin_retornarPartidoActualizadoEnLaBd() {
        PartidoVo partidoVo = new PartidoVo();
        partidoVo.setId(1l);
        partidoVo.setGolesLocal(2);
        partidoVo.setGolesVisitante(1);
        
        Prode prodeAntes = (Prode) jdbcTemplate.queryForObject("select * from prode where id = ?", new Object[]{1}, new BeanPropertyRowMapper(Prode.class));
        
        mundialController.actualizarResultadoPartido(partidoVo);
        
        Partido partidoDespues = (Partido) jdbcTemplate.queryForObject("select * from partido where id = ?", new Object[]{1}, new BeanPropertyRowMapper(Partido.class));
        Prode prodeDespues = (Prode) jdbcTemplate.queryForObject("select * from prode where id = ?", new Object[]{1}, new BeanPropertyRowMapper(Prode.class));
        
        Assert.assertEquals(2, partidoDespues.getGolesLocal());
        Assert.assertEquals(1, partidoDespues.getGolesVisitante());
        Assert.assertEquals(prodeAntes.getPuntos() + 5, prodeDespues.getPuntos());
    }

    private void login() throws AuthenticationException {
        Authentication request = new UsernamePasswordAuthenticationToken("usuario1@prodemundial.com", "123456");
        Authentication result = authenticationManager.authenticate(request);
        SecurityContextHolder.getContext().setAuthentication(result);
    }
}
