/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.domain;

import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author eestevez
 */
@Entity
public class Partido {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fecha;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "equipo_local_id")
    private Equipo local;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "equipo_visitante_id")
    private Equipo visitante;

    private int golesLocal;

    private int golesVisitante;

    @OneToMany(mappedBy = "prode", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Pronostico> prode;
    
    private Boolean partidoTerminado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Equipo getLocal() {
        return local;
    }

    public void setLocal(Equipo local) {
        this.local = local;
    }

    public Equipo getVisitante() {
        return visitante;
    }

    public void setVisitante(Equipo visitante) {
        this.visitante = visitante;
    }

    public int getGolesLocal() {
        return golesLocal;
    }

    public void setGolesLocal(int golesLocal) {
        this.golesLocal = golesLocal;
    }

    public int getGolesVisitante() {
        return golesVisitante;
    }

    public void setGolesVisitante(int golesVisitante) {
        this.golesVisitante = golesVisitante;
    }

    public List<Pronostico> getProde() {
        return prode;
    }

    public void setProde(List<Pronostico> prode) {
        this.prode = prode;
    }

    public Boolean isPartidoTerminado() {
        return partidoTerminado;
    }

    public void setPartidoTerminado(Boolean partidoTerminado) {
        this.partidoTerminado = partidoTerminado;
    }

}