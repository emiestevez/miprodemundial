/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.controller;

import com.prode.mundial.domain.Torneo;
import com.prode.mundial.service.TorneoService;
import com.prode.mundial.vo.TorneoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author eestevez
 */
@Controller
@RequestMapping(value = "/torneo")
public class TorneoController {

    @Autowired
    private TorneoService torneoService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Torneo buscar() {
        return torneoService.buscar();
    }

    @RequestMapping(value = "/ranking", method = RequestMethod.GET)
    @ResponseBody
    public Torneo buscarRanking() {
        return torneoService.buscarRanking();
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void crear(@RequestBody TorneoVo torneoVo) {
        torneoService.crear(torneoVo);
    }

}
