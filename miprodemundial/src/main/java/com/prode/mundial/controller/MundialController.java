/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.controller;

import com.prode.mundial.domain.Partido;
import com.prode.mundial.service.PartidoService;
import com.prode.mundial.service.ProdeService;
import com.prode.mundial.vo.PartidoVo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author eestevez
 */
@RequestMapping(value = "/mundial")
@Controller
public class MundialController {

    @Autowired
    private PartidoService partidoService;
    
    @Autowired
    private ProdeService prodeService;

    @RequestMapping(value = "/partidos/proximos")
    @ResponseBody
    public List<Partido> buscarProximosPartidos() {
        return partidoService.buscarProximosPartidos().subList(0, 3);
    }

    @RequestMapping(value="/partido/", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void actualizarResultadoPartido(PartidoVo partidoVo) {
        Partido partido = partidoService.actualizarResultado(partidoVo);
        prodeService.actualizarPuntos(partido);
    }
}
