/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prode.mundial.controller;

import com.prode.mundial.domain.Grupo;
import com.prode.mundial.service.GrupoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Emiliano
 */
@RequestMapping(value = "/grupo")
@Controller
public class GrupoController {
    
    @Autowired
    private GrupoService grupoService;
    
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Grupo> buscar(){
        return grupoService.buscar();
    }
}
