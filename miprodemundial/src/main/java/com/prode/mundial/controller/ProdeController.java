/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.controller;

import com.prode.mundial.domain.Prode;
import com.prode.mundial.domain.Pronostico;
import com.prode.mundial.service.ProdeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author eestevez
 */
@RequestMapping(value = "/prode")
@Controller
public class ProdeController {

    @Autowired
    private ProdeService prodeService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Prode buscar() {
        return prodeService.buscar();
    }

    @RequestMapping(value="/pronostico",method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void actualizarPronosticos(@RequestBody List<Pronostico> pronosticos) {
        prodeService.actualizarPronosticos(pronosticos);
    }

}
