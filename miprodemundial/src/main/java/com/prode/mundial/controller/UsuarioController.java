/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.controller;

import com.prode.mundial.domain.Usuario;
import com.prode.mundial.service.UsuarioService;
import com.prode.mundial.vo.UsuarioVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Emiliano
 */
@Controller
@RequestMapping(value = "/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Usuario guardar(@RequestBody UsuarioVo usuario) {
        return usuarioService.guardar(usuario);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Usuario buscar(){
        return usuarioService.buscar();
    }

    @RequestMapping(value = "/recuperar/{email}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void recuperar(@PathVariable String email) {
        usuarioService.recuperar(email);
    }
}
