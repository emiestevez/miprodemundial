/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prode.mundial.vo;

import java.util.List;

/**
 *
 * @author eestevez
 */
public class TorneoVo {
    private String nombre;
    
    private List<IntegranteVo> integrantes;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<IntegranteVo> getIntegrantes() {
        return integrantes;
    }

    public void setIntegrantes(List<IntegranteVo> integrantes) {
        this.integrantes = integrantes;
    }
    
}
