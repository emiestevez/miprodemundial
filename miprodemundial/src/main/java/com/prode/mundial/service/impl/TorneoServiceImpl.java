/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.service.impl;

import com.prode.mundial.domain.Prode;
import com.prode.mundial.domain.Torneo;
import com.prode.mundial.domain.Usuario;
import com.prode.mundial.repository.EmailRepository;
import com.prode.mundial.repository.ProdeRepository;
import com.prode.mundial.repository.TorneoRepository;
import com.prode.mundial.repository.UsuarioRepository;
import com.prode.mundial.service.TorneoService;
import com.prode.mundial.vo.IntegranteVo;
import com.prode.mundial.vo.TorneoVo;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author eestevez
 */
@Service
@Transactional
public class TorneoServiceImpl implements TorneoService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private ProdeRepository prodeRepository;

    @Autowired
    private TorneoRepository torneoRepository;
    
    @Autowired
    private EmailRepository emailRepository;

    @Autowired
    private StandardPasswordEncoder standardPasswordEncoder;

    @Override
    public Torneo buscarRanking() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Usuario usuario = usuarioRepository.findByEmail(authentication.getName());

        Prode prode = prodeRepository.findByUsuario(usuario);

        List<Prode> prodesDeLosUsuarios = prodeRepository.findByTorneoOrderByPuntosDesc(prode.getTorneo());
        Torneo torneo = new Torneo();
        torneo.setNombre(prode.getTorneo().getNombre());
        torneo.setProdes(prodesDeLosUsuarios);
        return torneo;
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public Torneo crear(TorneoVo torneoVo) {
        List<Usuario> listaDeUsuario = new ArrayList<Usuario>();
        
        
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Usuario usuarioLogueado = usuarioRepository.findByEmail(authentication.getName());

        Torneo torneo = new Torneo();
        torneo.setNombre(torneoVo.getNombre());

        torneo.setFundador(usuarioLogueado);
        Prode prodeDelUsuarioLogueado = prodeRepository.findByUsuario(usuarioLogueado);
        prodeDelUsuarioLogueado.setTorneo(torneo);
        prodeRepository.save(prodeDelUsuarioLogueado);

        Usuario usuario = null;
        for (IntegranteVo integranteVo : torneoVo.getIntegrantes()) {
            usuario = new Usuario();
            usuario.setNombre(integranteVo.getNombre());
            usuario.setPassword(standardPasswordEncoder.encode(RandomStringUtils.randomAlphanumeric(15)));
            usuario.setEmail(integranteVo.getEmail());
            usuario.setTorneo(torneo);
            usuario = usuarioRepository.save(usuario);
            listaDeUsuario.add(usuario);
            
            Prode prode = new Prode();
            prode.setUsuario(usuario);
            prode.setTorneo(torneo);
            prode.setPuntos(0);
            prodeRepository.save(prode);
        }

        torneo = torneoRepository.save(torneo);
        
        for (Usuario usuarioNuevo: listaDeUsuario){
            emailRepository.registracion(usuarioNuevo);
        }
        
        return torneo;
    }

    @Override
    public Torneo buscar() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Usuario usuario = usuarioRepository.findByEmail(authentication.getName());

        Prode prode = prodeRepository.findByUsuario(usuario);

        if (prode.getTorneo() == null) {
            throw new DataRetrievalFailureException("Torneo no creado");
        }

        return prode.getTorneo();
    }

}
