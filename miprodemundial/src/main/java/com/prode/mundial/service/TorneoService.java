/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prode.mundial.service;

import com.prode.mundial.domain.Torneo;
import com.prode.mundial.vo.TorneoVo;

/**
 *
 * @author eestevez
 */
public interface TorneoService {
    Torneo buscar();
    
    Torneo buscarRanking();
    
    Torneo crear(TorneoVo torneoVo);
}
