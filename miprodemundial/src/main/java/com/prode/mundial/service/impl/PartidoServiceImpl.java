/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.service.impl;

import com.prode.mundial.domain.Partido;
import com.prode.mundial.repository.PartidoRepository;
import com.prode.mundial.service.PartidoService;
import com.prode.mundial.vo.PartidoVo;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author eestevez
 */
@Service
public class PartidoServiceImpl implements PartidoService {

    @Autowired
    private PartidoRepository partidoRepository;

    @Override
    public List<Partido> buscarProximosPartidos() {
        return partidoRepository.findByFechaGreaterThan(DateUtils.addDays(new Date(), -1));
    }

    @Override
    public Partido actualizarResultado(PartidoVo partidoVo) {
        Partido partido = partidoRepository.findOne(partidoVo.getId());
        partido.setGolesLocal(partidoVo.getGolesLocal());
        partido.setGolesVisitante(partidoVo.getGolesVisitante());
        return partidoRepository.saveAndFlush(partido);
    }

}
