/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.service.impl;

import com.prode.mundial.domain.Mundial;
import com.prode.mundial.repository.MundialRepository;
import com.prode.mundial.service.MundialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Emiliano
 */
@Service
public class MundialServiceImpl implements MundialService {

    @Autowired
    private MundialRepository mundialRepository;
    
    @Override
    public Mundial buscar() {
        return mundialRepository.findOne(1l);
    }

}
