/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.service.impl;

import com.prode.mundial.domain.Grupo;
import com.prode.mundial.repository.GrupoRepository;
import com.prode.mundial.service.GrupoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author eestevez
 */
@Service
@Transactional
public class GrupoServiceImpl implements GrupoService {

    @Autowired
    private GrupoRepository grupoRepository;
    
    @Override
    public List<Grupo> buscar() {
        return grupoRepository.findAll();
    }

}
