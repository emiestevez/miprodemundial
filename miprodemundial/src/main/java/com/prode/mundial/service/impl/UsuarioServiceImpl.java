/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.service.impl;

import com.prode.mundial.domain.Prode;
import com.prode.mundial.domain.Rol;
import com.prode.mundial.domain.Torneo;
import com.prode.mundial.domain.Usuario;
import com.prode.mundial.repository.EmailRepository;
import com.prode.mundial.repository.RolRepository;
import com.prode.mundial.repository.TorneoRepository;
import com.prode.mundial.repository.UsuarioRepository;
import com.prode.mundial.service.ProdeService;
import com.prode.mundial.service.UsuarioService;
import com.prode.mundial.vo.UsuarioVo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Emiliano
 */
@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private ProdeService prodeService;
    
    @Autowired
    private TorneoRepository torneoRepository;

    @Autowired
    private StandardPasswordEncoder standardPasswordEncoder;
    
    @Autowired
    private RolRepository rolRepository;
    
    @Autowired
    private EmailRepository emailRepository;

    @Override
    public Usuario guardar(UsuarioVo usuarioVo) {
        Usuario usuario = new Usuario();
        usuario.setNombre(usuarioVo.getNombre());
        usuario.setEmail(usuarioVo.getEmail());
        usuario.setEnabled(true);
        usuario.setPassword(standardPasswordEncoder.encode(usuarioVo.getPassword()));
        usuario = usuarioRepository.save(usuario);
        
        Rol rol = new Rol();
        rol.setPermiso("user");
        rol.setUsuario(usuario);
        rolRepository.save(rol);
        
        
        Torneo torneo = new Torneo();
        torneo.setNombre(usuarioVo.getNombreTorneo());
        torneo.setFundador(usuario);
        torneoRepository.save(torneo);
        
        prodeService.guardar(usuario, torneo);
        
        emailRepository.registracion(usuario);
        
        return usuario;
    }

    @Override
    public Usuario buscar() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return usuarioRepository.findByEmail(authentication.getName());
    }

    @Override
    public Usuario buscar(String nombre) {
        return usuarioRepository.findByEmail(nombre);
    }

    @Override
    public void recuperar(String email) {
        Usuario usuario = usuarioRepository.findByEmail(email);
        if (usuario == null) {
            throw  new DataRetrievalFailureException("No existe usuario con esa dirección de correo electronico.");
        }
        emailRepository.recuperar(usuario);
    }

}
