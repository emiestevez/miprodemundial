/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prode.mundial.service;

import com.prode.mundial.domain.Usuario;
import com.prode.mundial.vo.UsuarioVo;

/**
 *
 * @author Emiliano
 */
public interface UsuarioService {

    Usuario guardar(UsuarioVo usuario);
    
    Usuario buscar(String nombre);
    
    Usuario buscar();
    
    void recuperar(String email);
}
