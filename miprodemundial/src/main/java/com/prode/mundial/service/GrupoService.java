/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prode.mundial.service;

import com.prode.mundial.domain.Grupo;
import java.util.List;

/**
 *
 * @author Emiliano
 */
public interface GrupoService {
    List<Grupo> buscar();
}
