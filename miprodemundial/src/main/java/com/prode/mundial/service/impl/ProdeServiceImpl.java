/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.service.impl;

import com.prode.mundial.domain.Grupo;
import com.prode.mundial.domain.Mundial;
import com.prode.mundial.domain.Partido;
import com.prode.mundial.domain.Prode;
import com.prode.mundial.domain.Pronostico;
import com.prode.mundial.domain.Torneo;
import com.prode.mundial.domain.Usuario;
import com.prode.mundial.repository.PartidoRepository;
import com.prode.mundial.repository.ProdeRepository;
import com.prode.mundial.repository.PronosticoRepository;
import com.prode.mundial.repository.UsuarioRepository;
import com.prode.mundial.service.MundialService;
import com.prode.mundial.service.ProdeService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author eestevez
 */
@Service
@Transactional
public class ProdeServiceImpl implements ProdeService {

    @Autowired
    private ProdeRepository prodeRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private PronosticoRepository pronosticoRepository;

    @Autowired
    private MundialService mundialService;

    @Override
    public Prode buscar() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Usuario usuario = usuarioRepository.findByEmail(authentication.getName());

        Mundial mundial = mundialService.buscar();

        Prode prode = prodeRepository.findByUsuario(usuario);

        for (Grupo grupo : mundial.getFaseDeGrupos()) {
            List<Pronostico> pronosticos = pronosticoRepository.findByProdeAndGrupo(prode, grupo);
            grupo.setPronosticos(pronosticos);
        }
        prode.setMundial(mundial);
        return prode;
    }

    @Override
    public Prode guardar(Usuario usuario, Torneo torneo) {
        Prode prode = new Prode();
        prode.setUsuario(usuario);
        prode.setPuntos(0);
        prode.setTorneo(torneo);

        prodeRepository.save(prode);

        return prode;
    }

    @Override
    @PreAuthorize("hasPermission(#pronosticos,'actualizar')")
    public void actualizarPronosticos(List<Pronostico> pronosticos) {
        for (Pronostico nuevoPronostico : pronosticos) {
            Pronostico pronostico = pronosticoRepository.findOne(nuevoPronostico.getId());
            pronostico.setGolesLocal(nuevoPronostico.getGolesLocal());
            pronostico.setGolesVisitante(nuevoPronostico.getGolesVisitante());
            pronosticoRepository.save(pronostico);
        }
    }

    @Override
    public void actualizarPuntos(Partido partido) {
        List<Prode> listaDeProdes = prodeRepository.findAll();

        for (Prode prode : listaDeProdes) {
            int puntos = prode.getPuntos();
            Pronostico pronostico = pronosticoRepository.findByProdeAndPartido(prode, partido);
            puntos += calcularPuntos(pronostico, partido);
            prode.setPuntos(puntos);
            prodeRepository.save(prode);
        }

    }

    private int calcularPuntos(Pronostico pronostico, Partido partido) {
        int puntos = 0;
        puntos += obtenerPuntosPorAcertarResultado(pronostico.getPartido(), partido);
        puntos += obtenerPuntosPorAcertarGolesDeUnEquipo(pronostico.getPartido(), partido);
        puntos += obtenerPuntosPorDiferenciaDeGoles(pronostico.getPartido(), partido);
        return puntos;
    }

    private int obtenerPuntosPorAcertarResultado(Partido partidoUsuario, Partido partidoOficial) {
        if ((partidoUsuario.getGolesLocal() > partidoUsuario.getGolesVisitante())
                && (partidoOficial.getGolesLocal() > partidoOficial.getGolesVisitante())) {
            return 2;
        }

        if ((partidoUsuario.getGolesLocal() < partidoUsuario.getGolesVisitante())
                && (partidoOficial.getGolesLocal() < partidoOficial.getGolesVisitante())) {
            return 2;
        }

        if ((partidoUsuario.getGolesLocal() == partidoUsuario.getGolesVisitante())
                && (partidoOficial.getGolesLocal() == partidoOficial.getGolesVisitante())) {
            return 2;
        }

        return 0;
    }

    private int obtenerPuntosPorAcertarGolesDeUnEquipo(Partido partidoUsuario, Partido partidoOficial) {
        int puntos = 0;
        if (partidoUsuario.getGolesLocal() == partidoOficial.getGolesLocal()) {
            puntos++;
        }

        if (partidoUsuario.getGolesVisitante() == partidoOficial.getGolesVisitante()) {
            puntos++;
        }

        return puntos;
    }

    private int obtenerPuntosPorDiferenciaDeGoles(Partido partidoUsuario, Partido partidoOficial) {
        int diferenciaGolesPartidoOficial = partidoOficial.getGolesLocal() - partidoOficial.getGolesVisitante();
        int diferenciaGolesPartidoUsuario = partidoUsuario.getGolesLocal() - partidoUsuario.getGolesVisitante();

        if (diferenciaGolesPartidoOficial == diferenciaGolesPartidoUsuario) {
            return 1;
        }

        return 0;
    }

}
