/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.service;

import com.prode.mundial.domain.Partido;
import com.prode.mundial.domain.Prode;
import com.prode.mundial.domain.Pronostico;
import com.prode.mundial.domain.Torneo;
import com.prode.mundial.domain.Usuario;
import java.util.List;

/**
 *
 * @author eestevez
 */
public interface ProdeService {

    Prode buscar();

    Prode guardar(Usuario usuario, Torneo torneo);
    
    void actualizarPronosticos(List<Pronostico> pronosticos);
    
    void actualizarPuntos(Partido partido);
}
