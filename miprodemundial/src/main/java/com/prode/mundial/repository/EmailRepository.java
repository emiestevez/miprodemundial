/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prode.mundial.repository;

import com.prode.mundial.domain.Usuario;

/**
 *
 * @author eestevez
 */
public interface EmailRepository {
    void registracion(final Usuario usuario);
    
    void recuperar(final Usuario usuario);
}
