/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.repository.impl;

import com.prode.mundial.domain.Usuario;
import com.prode.mundial.repository.EmailRepository;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.MimeMessage;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Repository;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 *
 * @author eestevez
 */
@Repository
public class EmailRepositoryImpl implements EmailRepository {

    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private VelocityEngine velocityEngine;
    @Value(value = "${prode.mail.from}")
    private String from;
    @Value(value = "${prode.mail.registracion.asunto}")
    private String registracionAsunto;
    @Value(value = "${prode.mail.encoding}")
    private String encoding;

    @Override
    public void registracion(final Usuario usuario) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
                message.setTo(usuario.getEmail());
                message.setFrom(from);
                Map model = new HashMap();
                model.put("nombre", usuario.getNombre());
                model.put("email", usuario.getEmail());
                String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/prode/mundial/repository/registracion.vm", encoding, model);
                message.setText(text, true);
                message.setSubject(registracionAsunto);

                Resource resource = new FileSystemResource("c:/workspace/miprodemundial/miprodemundial/src/main/resources/com/prode/mundial/repository/header.png");
                FileSystemResource res = new FileSystemResource(resource.getFile());
                message.addInline("logo", res);
            }
        };

        mailSender.send(preparator);
    }

    @Override
    public void recuperar(final Usuario usuario) {
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
                message.setTo(usuario.getEmail());
                message.setFrom(from);
                Map model = new HashMap();
                model.put("password", usuario.getPassword());
                String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "com/prode/mundial/repository/recuperar.vm", encoding, model);
                message.setText(text, true);
                message.setSubject(registracionAsunto);

                Resource resource = new FileSystemResource("c:/workspace/miprodemundial/miprodemundial/src/main/resources/com/prode/mundial/repository/header.png");
                FileSystemResource res = new FileSystemResource(resource.getFile());
                message.addInline("logo", res);
            }
        };

        mailSender.send(preparator);
    }

}
