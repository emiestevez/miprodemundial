/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.prode.mundial.repository;

import com.prode.mundial.domain.Grupo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author eestevez
 */
public interface GrupoRepository extends JpaRepository<Grupo, Long>{
    
}
