/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prode.mundial.repository;

import com.prode.mundial.domain.Grupo;
import com.prode.mundial.domain.Partido;
import com.prode.mundial.domain.Prode;
import com.prode.mundial.domain.Pronostico;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author eestevez
 */
public interface PronosticoRepository extends JpaRepository<Pronostico, Long> {
    List<Pronostico> findByProdeAndGrupo(Prode prode, Grupo grupo);
    
    Pronostico findByIdAndProde(Long id,Prode prode);
    
    Pronostico findByProdeAndPartido(Prode prode, Partido partido);
}
