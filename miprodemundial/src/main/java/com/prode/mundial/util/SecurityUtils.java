package com.prode.mundial.util;

import com.prode.mundial.domain.Grupo;
import com.prode.mundial.domain.Prode;
import com.prode.mundial.domain.Pronostico;
import com.prode.mundial.domain.Usuario;
import com.prode.mundial.repository.ProdeRepository;
import com.prode.mundial.repository.PronosticoRepository;
import com.prode.mundial.repository.UsuarioRepository;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Clase utilitaria con operaciones comunes sobre aspectos de seguridad.
 */
@SuppressWarnings("PMD")
@Service(value = "prodeSecurity")
public class SecurityUtils implements PermissionEvaluator {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private ProdeRepository prodeRepository;

    @Autowired
    private PronosticoRepository pronosticoRepository;

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        if (targetDomainObject instanceof List && permission.equals("actualizar")) {
            return hasPermissionActualizarProde((List<Pronostico>) targetDomainObject);
        }

        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        return false;
    }

    private boolean hasPermissionActualizarProde(List<Pronostico> pronosticos) {
        Usuario usuario = usuarioRepository.findByEmail(getUsername());
        Prode prode = prodeRepository.findByUsuario(usuario);

        for (Pronostico pronostico : pronosticos) {
            Pronostico pronosticoDelUsuario = pronosticoRepository.findOne(pronostico.getId());
            if (prode.getId() != pronosticoDelUsuario.getProde().getId()){
                return false;
            }
        }

        return true;
    }

    public static String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication == null ? null : authentication.getName();
    }

}
