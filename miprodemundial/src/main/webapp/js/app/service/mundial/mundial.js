prode.service.mundial = function() {
    function buscarProximosPartidos() {
        var uri = prode.service.url() + "mundial/partidos/proximos";
        return prode.service.get(uri);
    }

    return {
        buscarProximosPartidos: buscarProximosPartidos
    };
}();