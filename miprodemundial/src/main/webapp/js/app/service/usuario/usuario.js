prode.service.usuario = function() {
    function nuevo(usuario) {
        var uri = prode.service.url() + "usuario";
        return prode.service.post(uri, usuario);
    }

    function buscar(){
        var uri = prode.service.url() + "usuario";
        return prode.service.get(uri);
    }
    
    function recuperar(email) {
        var uri = prode.service.url() + "usuario/recuperar/" + email;
        return prode.service.get(uri);
    }

    return {
        nuevo: nuevo,
        buscar: buscar,
        recuperar: recuperar
    };
}();