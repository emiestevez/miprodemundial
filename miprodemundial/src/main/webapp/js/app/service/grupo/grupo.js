prode.service.grupo = function() {
    function buscarTodos() {
        var uri = prode.service.url() + "grupo";
        return prode.service.get(uri);
    }

    return {
        buscarTodos: buscarTodos
    };
}();