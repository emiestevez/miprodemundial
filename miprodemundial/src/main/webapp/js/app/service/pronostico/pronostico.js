prode.service.pronostico = function() {
    function buscar() {
        var uri = prode.service.url() + "prode";
        return prode.service.get(uri);
    }
    
    function actualizar(data) {
        var uri = prode.service.url() + "prode/pronostico";
        return prode.service.put(uri, data);
    }

    return {
        buscar: buscar,
        actualizar: actualizar
    };
}();