prode.service = function() {
    function uriService() {
        var url = prode.url();
        return url + "api/";
    }

    function post(uri, data) {
        return $.ajax({
            type: 'post',
            url: uri,
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8"
        });
    }

    function put(uri, data) {
        return $.ajax({
            type: 'put',
            url: uri,
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8"
        });
    }

    function get(uri) {
        return $.ajax({
            type: 'get',
            url: uri,
            contentType: "application/json; charset=utf-8"
        });
    }

    function erase(uri) {
        return $.ajax({
            type: 'delete',
            url: uri,
            contentType: "application/json; charset=utf-8"
        });
    }


    return {
        url: uriService,
        get: get,
        post:post,
        put: put
    };
}();