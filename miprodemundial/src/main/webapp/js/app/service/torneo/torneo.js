prode.service.torneo = function() {
    function buscarRanking() {
        var uri = prode.service.url() + "torneo/ranking";
        return prode.service.get(uri);
    }
    
    function crear(torneo) {
        var uri = prode.service.url() + "torneo";
        return prode.service.post(uri, torneo);
    }
    
    function editar(torneo) {
        var uri = prode.service.url() + "torneo";
        return prode.service.put(uri, torneo);
    }
    
    function buscar() {
        var uri = prode.service.url() + "torneo";
        return prode.service.get(uri);
    }

    return {
        buscarRanking: buscarRanking,
        crear: crear,
        editar: editar,
        buscar: buscar
    };
}();