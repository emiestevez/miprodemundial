prode.ui.login.registro = (function() {
    
    var message = {msg:""};

    function init() {
        $("#registro").on("click", nuevaInscripcion);
        $.templates("#mensajeAlertaTemplate").link("#mensajeLogin", message);
    }

    function nuevaInscripcion() {
        if ($("#registroPassword").val() !== $("#registroPasswordValidar").val()){
            $.observable(message).setProperty({msg:"las contraseñas deben iguales"});
            return;
        }
        
        var usuarioNuevo = {};
        usuarioNuevo.nombre = $("#registroNombre").val();
        usuarioNuevo.password = $("#registroPassword").val();
        usuarioNuevo.email = $("#registroEmail").val();
        prode.service.usuario.nuevo(usuarioNuevo).done(mostrarMensajeUsuarioNuevo).fail(mostrarMensajeUsuarioError);
    }
    
    function mostrarMensajeUsuarioNuevo(){
        $.observable(message).setProperty({msg:"Usuario creado con éxito"});
    }
    
    function mostrarMensajeUsuarioError(){
        $.observable(message).setProperty({msg:"No se pudo crear el usuario. Por favor vuelva a intentarlo nuevamente."});
    }

    return {
        init: init
    };
})();
