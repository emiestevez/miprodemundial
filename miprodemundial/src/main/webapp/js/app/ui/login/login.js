prode.ui.login = (function(){
    
    function init(){
        prode.ui.login.registro.init();
    }
    
    return {
        init: init
    };
})();

$(document).ready(function(){
   prode.ui.login.init(); 
});