prode.ui.torneo.editar = (function() {

    function init() {
        prode.service.torneo.buscar().done(mostrarTorneo).fail(errorAlBuscarTorneo);
        $("#guardar").on("click", guardarTorneo);

    }

    function guardarTorneo() {
        var integrantes = [];
        $.each($(".prode-js-integrante"), function(index, value) {

            if (($(this).find(".prode-js-integrante-nombre").val() !== "" && 
                    $(this).find(".prode-js-integrante-nombre").val() !== undefined) &&
                    ($(this).find(".prode-js-integrante-email").val() !== "" &&
                    $(this).find(".prode-js-integrante-email").val() !== undefined)) {
                var integrante = {};
                integrante.nombre = $(this).find(".prode-js-integrante-nombre").val();
                integrante.email = $(this).find(".prode-js-integrante-email").val();
                integrantes.push(integrante);
            }
        });

        var torneo = {};
        torneo.nombre = $("#nombre").val();
        torneo.integrantes = integrantes;
        prode.service.torneo.crear(torneo).done(mensajeTorneoCreadoConExito).fail(mensajeErrorAlCrearTorneo);
    }

    function mensajeTorneoCreadoConExito() {
        prode.ui.torneo.setMensaje({msg: "Se creo con exito el torneo. A jugar!!!"});
    }

    function mensajeErrorAlCrearTorneo() {
        prode.ui.torneo.setMensaje({msg: "Error al crear el torneo. Por favor intente más tarde."});
    }

    function mostrarTorneo(torneo) {
        $.templates("#torneoTemplate").link("torneo", torneo);
    }

    function errorAlBuscarTorneo(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status !== 404) {
            prode.ui.torneo.setMensaje({msg: "Error al obtener el torneo. Por favor intente más tarde."});
        } else {

        }
    }

    return {
        init: init
    };
})();
