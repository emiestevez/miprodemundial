prode.ui.torneo = (function(){
    var mensaje = {msg:""};
    
    function init(){
        prode.ui.torneo.editar.init();
        $.templates("#mensajeTemplate").link("#mensajePantallaTorneo", mensaje);
    }
    
    function setMensaje(nuevoMensaje) {
        $.observable(mensaje).setProperty(nuevoMensaje);
    }
    
    return {
        init: init,
        setMensaje: setMensaje
    };
})();

$(document).ready(function(){
   prode.ui.torneo.init(); 
});