prode.ui.registro = (function() {
    var mensaje = {type:"",msg: ""};
    var spinner = {msg: ""};

    function init() {
        $.templates("#mensajeAlertaTemplate").link("#mensajeLogin", mensaje);
        $.templates("#spinnerTemplate").link("#spinnerPantallaRegistrar", spinner);
        prode.ui.registro.usuario.init();
    }

    function setMensaje(type, nuevoMensaje) {
        $.observable(mensaje).setProperty({type: type, msg: nuevoMensaje});
    }
    
    function setMensajeSpinner(mensajeNuevo) {
        $.observable(spinner).setProperty(mensajeNuevo);
    }

    return {
        init: init,
        setMensaje: setMensaje,
        setMensajeSpinner: setMensajeSpinner
    };
})();

$(document).ready(function() {
    prode.ui.registro.init();
});