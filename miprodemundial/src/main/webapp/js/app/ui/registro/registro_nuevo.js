prode.ui.registro.usuario = (function(){
    
    function init() {
        $("#registro").on("click", nuevaInscripcion);
    }

    function nuevaInscripcion() {
        var usuarioNuevo = {};
        
        usuarioNuevo.nombre = $("#nombre").val();
        usuarioNuevo.password = $("#password").val();
        usuarioNuevo.email = $("#email").val();
        usuarioNuevo.nombreTorneo = $("#nombreTorneo").val();
        
        if (!esNuevoUsuarioValido(usuarioNuevo)){
            return;
        }
        
        prode.ui.registro.setMensajeSpinner({msg: "Cargando..."});
        prode.service.usuario.nuevo(usuarioNuevo).done(mostrarMensajeUsuarioNuevo).fail(mostrarMensajeUsuarioError);
    }
    
    function esNuevoUsuarioValido(usuarioNuevo) {
        if (usuarioNuevo.nombre.length < 1) {
            prode.ui.registro.setMensaje("danger","Completa el nombre");
            $("#nombre").focus();
            return false;
        }
        
        if ($("#password").val() !== $("#passwordReply").val()){
            prode.ui.registro.setMensaje("danger","Las contraseñas deben ser iguales");
            $("#password").focus();
            return false;
        }
        
        if (usuarioNuevo.email.length < 1) {
            prode.ui.registro.setMensaje("danger","Completa el email");
            $("#email").focus();
            return false;
        }
        
        if (usuarioNuevo.nombreTorneo.length < 1) {
             prode.ui.registro.setMensaje("danger","Completa el nombre del torneo");
            $("#nombreTorneo").focus();
            return false;
        }
        
        return true;
    }
    
    function mostrarMensajeUsuarioNuevo(){
        prode.ui.registro.setMensajeSpinner({msg: ""});
        prode.ui.registro.setMensaje("success","Se creo el usuario y el torneo con éxito");
    }
    
    function mostrarMensajeUsuarioError(){
        prode.ui.registro.setMensajeSpinner({msg: ""});
        prode.ui.registro.setMensaje("danger","No se pudo crear el usuario. Por favor vuelva a intentarlo nuevamente.");
    }

    return {
        init: init
    };
})();
