prode.ui.index = (function() {

    function init() {
        $("#diasParaElInicioDelMundial").countdown({until: new Date(2014, 6 - 1, 12), description: "Falta para que inice el mundial"});
    }

    return {
        init: init
    };
})();

$(document).ready(function() {
    prode.ui.index.init();
});