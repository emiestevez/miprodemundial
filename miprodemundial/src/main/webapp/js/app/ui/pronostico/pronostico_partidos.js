prode.ui.pronostico.partidos = (function() {

    function init() {
        prode.ui.pronostico.setMensajeSpinner({msg: "Cargando..."});
        prode.service.pronostico.buscar().done(mostrarPronostico);
    }

    function mostrarPronostico(fixture) {
        $.templates("#prodeTemplate").link("#mispronosticos", fixture);
        prode.ui.pronostico.setMensajeSpinner({msg: ""});
    }

    return {
        init: init
    };
})();

