prode.ui.pronostico.guardar = (function() {
    var mensaje = {msg: ""};

    function init() {
        $("#guardarPronostico").on("click", guardarPronostico);
        $.templates("#mensajeAlertaTemplate").link("#pronosticoMensajes", mensaje);
    }

    function guardarPronostico() {
        var grupos = $(".prode-js-pronosticos-grupo");
        var listaDePronosticos = [];

        $.each(grupos, function(index, value) {
            var nuevosPronosticos = $(value).view().data.pronosticos;
            for (var i = 0; i < nuevosPronosticos.length; i++) {
                nuevosPronosticos[i].golesLocal = parseInt(nuevosPronosticos[i].golesLocal);
                nuevosPronosticos[i].golesVisitante = parseInt(nuevosPronosticos[i].golesVisitante);
            }
            listaDePronosticos = listaDePronosticos.concat(nuevosPronosticos);
        });

        prode.service.pronostico.actualizar(listaDePronosticos).done(pronosticoGuardado).fail(pronosticoNoGuardado);
    }

    function pronosticoGuardado() {
        $.observable(mensaje).setProperty({msg: "Se guardo con exito los pronosticos"});
    }

    function pronosticoNoGuardado() {
        $.observable(mensaje).setProperty({msg: "No se pudo guardar los pronosticos. Intente más tarde por favor."});
    }

    return {
        init: init
    };
})();

