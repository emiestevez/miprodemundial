prode.ui.pronostico = (function() {

    var mensaje = {msg: ""};
    var spinner = {msg: ""};

    function init() {
        $.views.helpers({
            diaDelPartido: function(fecha) {
                var anio = parseInt(fecha.substr(0, 4));
                var mes = parseInt(fecha.substr(5, 2)) - 1;
                var dia = parseInt(fecha.substr(8, 2));
                return $.datepicker.formatDate('d/mm', new Date(anio, mes, dia));
            }
        });

        $.templates("#mensajeAlertaTemplate").link("#pronosticoMensajes", mensaje);
        $.templates("#spinnerTemplate").link("#spinnerPantallaMisPronosticos", spinner);
        prode.ui.pronostico.partidos.init();
        prode.ui.pronostico.guardar.init();
    }

    function setMensaje(mensajeNuevo) {
        $.observable(mensaje).setProperty(mensajeNuevo);
    }

    function setMensajeSpinner(mensajeNuevo) {
        $.observable(spinner).setProperty(mensajeNuevo);
    }

    return {
        init: init,
        setMensaje: setMensaje,
        setMensajeSpinner: setMensajeSpinner
    };
})();

$(document).ready(function() {
    prode.ui.pronostico.init();
});