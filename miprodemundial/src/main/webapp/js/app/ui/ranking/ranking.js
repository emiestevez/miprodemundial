prode.ui.ranking = (function() {

    function init() {
        prode.service.torneo.buscarRanking().done(mostrarRanking);

    }

    function mostrarRanking(ranking) {
        $.templates("#rankingTorneoTemplate").link("#rankingTorneo", ranking);
    }

    return {
        init: init
    };
})();

$(document).ready(function() {
    prode.ui.ranking.init();
});