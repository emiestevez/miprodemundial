prode.ui.home = (function() {
    var mensaje = {msg: ""};
    var spinner = {msg: ""};

    function init() {
        $.views.helpers({
            diaDeLaSemana: function(fecha) {
                var anio = parseInt(fecha.substr(0, 4));
                var mes = parseInt(fecha.substr(5, 2)) - 1;
                var dia = parseInt(fecha.substr(8, 2));
                return $.datepicker.formatDate('DD', new Date(anio, mes, dia));
            },
            dia: function(fecha) {
                var anio = parseInt(fecha.substr(0, 4));
                var mes = parseInt(fecha.substr(5, 2)) - 1;
                var dia = parseInt(fecha.substr(8, 2));
                return $.datepicker.formatDate('d', new Date(anio, mes, dia));
            },
            mes: function(fecha) {
                var anio = parseInt(fecha.substr(0, 4));
                var mes = parseInt(fecha.substr(5, 2)) - 1;
                var dia = parseInt(fecha.substr(8, 2));
                return $.datepicker.formatDate('MM', new Date(anio, mes, dia));
            },
            hora: function(fecha) {
                var anio = parseInt(fecha.substr(0, 4));
                var mes = parseInt(fecha.substr(5, 2));
                var dia = parseInt(fecha.substr(8, 2));
                return $.datepicker.formatDate('DD', new Date(anio, mes, dia));
            }
        });
        $.templates("#mensajeTemplate").link("#mensajePantallaHome", mensaje);
        $.templates("#spinnerTemplate").link("#spinnerPantallaMisPronosticos", spinner);
        prode.ui.home.ranking.init();
        prode.ui.home.partidos.init();
    }

    function setMensaje(nuevoMensaje) {
        $.observable(mensaje).setProperty(nuevoMensaje);
    }

    function setMensajeSpinner(mensajeNuevo) {
        $.observable(spinner).setProperty(mensajeNuevo);
    }

    return {
        init: init,
        setMensaje: setMensaje,
        setMensajeSpinner: setMensajeSpinner
    };
})();

$(document).ready(function() {
    prode.ui.home.init();
});