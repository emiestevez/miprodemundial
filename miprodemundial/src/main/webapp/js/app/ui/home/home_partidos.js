prode.ui.home.partidos = (function() {

    function init() {
        prode.service.mundial.buscarProximosPartidos().done(mostrarProximosPartidos).fail(errorAlBuscarProximosPartidos);
    }

    function mostrarProximosPartidos(partidos) {
        $.templates("#proximosPartidosTemplate").link("#proximosPartidos", partidos);
    }

    function errorAlBuscarProximosPartidos(jqXHR, textStatus, errorThrown) {
        prode.ui.torneo.setMensaje({msg: "Ocurrio un error. Por favor intente más tarde."});
    }

    return {
        init: init
    };
})();