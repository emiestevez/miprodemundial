prode.ui.home.ranking = (function() {

    function init() {
        prode.ui.home.setMensajeSpinner({msg: "Cargando..."});
        prode.service.torneo.buscar().done(mostrarTorneo).fail(errorAlBuscarTorneo);
    }

    function mostrarTorneo(torneo) {
        $.templates("#torneoTemplate").link("#torneo", torneo);
        prode.ui.home.setMensajeSpinner({msg: ""});
    }

    function errorAlBuscarTorneo(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status !== 404) {
            prode.ui.torneo.setMensaje({msg: "Error al obtener el torneo. Por favor intente más tarde."});
        } else {
            prode.ui.torneo.setMensaje({msg: "Todavia no creaste el torneo. Hacelo haciendo click en crear"});
        }
    }

    return {
        init: init
    };
})();