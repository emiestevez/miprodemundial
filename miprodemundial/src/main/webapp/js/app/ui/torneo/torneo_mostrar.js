prode.ui.torneo.mostrar = (function() {
    var amigos = [];

    function init() {
        prode.ui.torneo.setMensajeSpinner({msg: "Cargando..."});
        prode.service.torneo.buscar().done(mostrarTorneo).fail(errorAlBuscarTorneo);
    }

    function mostrarTorneo(torneo) {
        amigos = torneo.prodes;
        $.templates("#torneoTemplate").link("#miembrosDelTorneo", amigos);
        prode.ui.torneo.setMensajeSpinner({msg: ""});
        prode.ui.torneo.editar.init();
    }

    function errorAlBuscarTorneo(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status !== 404) {
            prode.ui.torneo.setMensaje({msg: "Error al obtener el torneo. Por favor intente más tarde."});
        } else {
            prode.ui.torneo.setMensaje({msg: "Todavia no creaste el torneo. Hacelo haciendo click en crear"});
        }
    }
    
    function agregarAmigos(amigosNuevo) {
        $.observable(amigos).insert(0, amigosNuevo);
    }

    return {
        init: init,
        agregarAmigos: agregarAmigos
    };
})();
