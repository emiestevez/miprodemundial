prode.ui.torneo.guardar = (function() {

    function init() {
        $("#guardarTorneo").on("click", guardarTorneo);
    }

    function guardarTorneo() {
        var integrantes = [];
        $.each($(".prode-js-integrante"), function(index, value) {

            if (($(this).find(".prode-js-integrante-nombre").val() !== "" && 
                    $(this).find(".prode-js-integrante-nombre").val() !== undefined) &&
                    ($(this).find(".prode-js-integrante-email").val() !== "" &&
                    $(this).find(".prode-js-integrante-email").val() !== undefined)) {
                var integrante = {};
                integrante.nombre = $(this).find(".prode-js-integrante-nombre").val();
                integrante.email = $(this).find(".prode-js-integrante-email").val();
                integrantes.push(integrante);
            }
        });

        var torneo = {};
        torneo.nombre = $("#nombre").val();
        torneo.integrantes = integrantes;
        prode.service.torneo.crear(torneo).done(mensajeTorneoCreadoConExito).fail(mensajeErrorAlCrearTorneo);
    }

    function mensajeTorneoCreadoConExito() {
        prode.ui.torneo.setMensaje({msg: "Se creo con exito el torneo. A jugar!!!"});
    }

    function mensajeErrorAlCrearTorneo() {
        prode.ui.torneo.setMensaje({msg: "Error al crear el torneo. Por favor intente más tarde."});
    }

    return {
        init: init
    };
})();
