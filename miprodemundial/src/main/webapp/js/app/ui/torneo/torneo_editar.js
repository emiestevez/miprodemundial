prode.ui.torneo.editar = (function() {

    function init() {
        $("#editarTorneo").on("click", editarTorneo);
    }

    function editarTorneo() {
        $(".prode-js-nombre-torneo").editable({
            type: 'text',
            pk: 1,
            title: 'Modifica el nombre del torneo',
            mode: 'inline'
        });
    }

    return {
        init: init
    };
})();
