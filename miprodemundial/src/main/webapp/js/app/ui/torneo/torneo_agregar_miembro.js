prode.ui.torneo.miembro = (function() {

    function init() {
        $("#nuevoAmigo").on("click", agregarNuevoAmigo);
    }
    
    function agregarNuevoAmigo(){
        
        var prodes = {};
        prodes.puntos = 0;
        var usuario = {};
        usuario.nombre = $(".prode-js-nombre-amigo").val();
        usuario.email = $(".prode-js-email-amigo").val();
        prodes.usuario = usuario;
        
        prode.ui.torneo.mostrar.agregarAmigos(prodes);
        
        $(".prode-js-nombre-amigo").val("");
        $(".prode-js-email-amigo").val("");
        $(".prode-js-nombre-amigo").focus();
    }

    return {
        init: init
    };
})();
