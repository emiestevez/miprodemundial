prode.ui.torneo = (function(){
    var mensaje = {msg:""};
    var spinner = {msg:""};
    
    function init(){
        $.templates("#mensajeTemplate").link("#mensajePantallaTorneo", mensaje);
        $.templates("#spinnerTemplate").link("#spinnerPantallaMisPronosticos", spinner);
        prode.ui.torneo.mostrar.init();
        prode.ui.torneo.miembro.init();
        prode.ui.torneo.guardar.init();
    }
    
    function setMensaje(nuevoMensaje) {
        $.observable(mensaje).setProperty(nuevoMensaje);
    }
    
    function setMensajeSpinner(mensajeNuevo) {
        $.observable(spinner).setProperty(mensajeNuevo);
    }
    
    return {
        init: init,
        setMensaje: setMensaje,
        setMensajeSpinner: setMensajeSpinner
    };
})();

$(document).ready(function(){
   prode.ui.torneo.init(); 
});