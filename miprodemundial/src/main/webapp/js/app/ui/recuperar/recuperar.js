prode.ui.recuperar = (function() {
    var mensaje = {type:"",msg: ""};
    var spinner = {msg: ""};

    function init() {
        $.templates("#mensajeAlertaTemplate").link("#mensajeRecuperar", mensaje);
        $.templates("#spinnerTemplate").link("#spinnerPantallaRecuperar", spinner);
        prode.ui.recuperar.usuario.init();
    }

    function setMensaje(type, nuevoMensaje) {
        $.observable(mensaje).setProperty({type: type, msg: nuevoMensaje});
    }
    
    function setMensajeSpinner(mensajeNuevo) {
        $.observable(spinner).setProperty(mensajeNuevo);
    }

    return {
        init: init,
        setMensaje: setMensaje,
        setMensajeSpinner: setMensajeSpinner
    };
})();

$(document).ready(function() {
    prode.ui.recuperar.init();
});