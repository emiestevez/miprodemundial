prode.ui.recuperar.usuario = (function() {

    function init() {
        $("#recuperar").on("click", recuperarContraseña);
    }

    function recuperarContraseña() {
        prode.ui.recuperar.setMensajeSpinner({msg: "Cargando..."});
        prode.service.usuario.recuperar($("#email").val()).done(mostrarMensajeRecuperarContraseñaOk).fail(mostrarMensajeRecuperarContraseñaError);
    }

    function mostrarMensajeRecuperarContraseñaOk() {
        prode.ui.recuperar.setMensajeSpinner({msg: ""});
        prode.ui.recuperar.setMensaje("success", "Se envio a tu casilla de email la contraseña.");
    }

    function mostrarMensajeRecuperarContraseñaError(jqXHR, textStatus, errorThrown) {
        prode.ui.recuperar.setMensajeSpinner({msg: ""});
        if (jqXHR.status === 404) {
            prode.ui.recuperar.setMensaje("danger", "No existe ning&uacute;n usuario con la direcci&oacute;n " + $("#email").val());
        } else {
            prode.ui.recuperar.setMensaje("danger", "No se pudo obtener la contraseña. Intente más tarde por favor.");
        }
    }

    return {
        init: init
    };
})();

