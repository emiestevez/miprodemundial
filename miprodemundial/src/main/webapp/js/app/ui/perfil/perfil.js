prode.ui.perfil = (function(){
    var mensaje = {msg:""};
    var spinner = {msg:""};
    
    function init(){
        $.templates("#mensajeTemplate").link("#mensajePantallaTorneo", mensaje);
        $.templates("#spinnerTemplate").link("#spinner", spinner);
        prode.ui.perfil.mostrar.init();
        prode.ui.perfil.editar.init();
    }
    
    function setMensaje(nuevoMensaje) {
        $.observable(mensaje).setProperty(nuevoMensaje);
    }
    
    function setMensajeSpinner(mensajeNuevo) {
        $.observable(spinner).setProperty(mensajeNuevo);
    }
    
    return {
        init: init,
        setMensaje: setMensaje,
        setMensajeSpinner: setMensajeSpinner
    };
})();

$(document).ready(function(){
   prode.ui.perfil.init(); 
});