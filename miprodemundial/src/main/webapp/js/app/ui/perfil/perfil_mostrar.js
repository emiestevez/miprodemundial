prode.ui.perfil.mostrar = (function() {

    function init() {
        prode.ui.perfil.setMensajeSpinner({msg: "Cargando..."});
        prode.service.usuario.buscar().done(mostrarPerfil);
    }
    
    function mostrarPerfil(usuario) {
        prode.ui.perfil.setMensajeSpinner({msg: ""});
        $.templates("#perfilTemplate").link("#perfil", usuario);
        
    }
    
    return {
        init: init
    };
})();
