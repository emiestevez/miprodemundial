prode.ui.perfil.editar = (function() {

    var nombre = "";

    function init() {
        $(".prode-js-perfil-editar").on("click", habilitarEditar);
    }

    function habilitarEditar() {
        $(".prode-js-perfil-editar, .prode-js-perfil-nombre").addClass("hide");
        $(".prode-js-perfil-cancelar, .prode-js-perfil-guardar, .prode-js-perfil-editar-nombre").removeClass("hide");
        
        var datosFormularioPerfil = $(".prode-js-perfil").view().data;
        nombre = datosFormularioPerfil.nombre;
        
        $(".prode-js-perfil-editar-nombre").focus();
        
        $(".prode-js-perfil-cancelar").on("click", cancelarCambios);
        $(".prode-js-perfil-guardar").on("click", guardarCambios);
    }
    
    function cancelarCambios() {
        var formularioPerfil = $(".prode-js-perfil").view();
        formularioPerfil.data.nombre = nombre;
        formularioPerfil.refresh();
        
        $(".prode-js-perfil-editar, .prode-js-perfil-nombre").removeClass("hide");
        $(".prode-js-perfil-cancelar, .prode-js-perfil-guardar, .prode-js-perfil-editar-nombre").addClass("hide");
        $(".prode-js-perfil-editar").on("click", habilitarEditar);
    }
    
    function guardarCambios() {
        var formularioPerfil = $(".prode-js-perfil").view();
        formularioPerfil.refresh();
        
        $(".prode-js-perfil-editar, .prode-js-perfil-nombre").removeClass("hide");
        $(".prode-js-perfil-cancelar, .prode-js-perfil-guardar, .prode-js-perfil-editar-nombre").addClass("hide");
        $(".prode-js-perfil-editar").on("click", habilitarEditar);
    }
    
    return {
        init: init
    };
})();
