<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="business, corporate, corporate website, creative, html5, marketing, multipurpose, responsive, site templates">
        <link rel="shortcut icon" href="img/favicon.png">
        <title>Mi prode mundial</title>

        <!-- css -->
        <link href="css/template/theme-style.css" rel="stylesheet"> 
        <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">      
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/bootstrap-editable.css" rel="stylesheet">
        <link href="css/jquery.countdown.css" rel="stylesheet">
        <link href="css/sticky-footer.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
        <link href="css/miprodemundial.home.css" rel="stylesheet">
    </head>
    <body data-spy="scroll" data-target="#ha-header">
        
        <tiles:insertAttribute name="body"/>

        <!-- JavaScript --> 
        <script src="js/lib/jquery-1.10.2.min.js"></script> 
        <script src="js/lib/bootstrap.min.js"></script> 
        <script src="js/lib/nicescroll.min.js"></script><!-- jquery nice scroll--> 
        <script src="js/lib/pace.min.js" ></script> <!--page load progress bar--> 
        <script src="js/lib/jquery.validate.min.js"></script><!--contact page--> 
        <script src="js/lib/isotope.min.js"></script><!--Portfolio Filter--> 
        <script src="js/lib/flexslider.min.js"></script><!-- FlexSlider --> 
        <script src="js/lib/waypoints.min.js"></script><!--Header Effect--> 
        <script src="js/lib/custom_min.js"></script><!-- Custom JavaScript  -->

    </body>
</html>