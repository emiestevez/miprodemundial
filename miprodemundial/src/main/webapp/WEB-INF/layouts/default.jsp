<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <c:set var="req" value="${pageContext.request}" />
        <c:set var="uri" value="${req.requestURI}" />
        <base href="${fn:replace(req.requestURL, fn:substring(uri, 0, fn:length(uri)), req.contextPath)}/"/>
        <meta charset="utf-8">
        <title><tiles:getAsString name="title"/></title>

        <link rel="icon" type="image/png" href="img/icons/favicon.ico" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">

        <!-- css -->
        <link href="css/template/theme-style.css" rel="stylesheet"> 
        <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">      
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/bootstrap-editable.css" rel="stylesheet">
        <link href="css/jquery.countdown.css" rel="stylesheet">
        <link href="css/sticky-footer.css" rel="stylesheet">
        <!-- css miprodemundial -->
        <link href="css/miprodemundial.css" rel="stylesheet"> 
    </head>
    <body>
        <tiles:insertAttribute name="top"/>
        <tiles:insertAttribute name="header"/>
        <tiles:insertAttribute name="menu"/>

        <div class="container prode-container">
            <tiles:insertAttribute name="body"/>
        </div>

        <tiles:insertAttribute name="footer"/>

        <script id="mensajeAlertaTemplate" type="text/x-jsrender">
            {^{if msg}}
                <div class="alert alert-{{>type}} alert-dismissable prode-mensajes">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{:msg}}
                </div>
            {{/if}}
        </script>
        
        <script id="spinnerTemplate" type="text/x-jsrender">
            {^{if msg}}
                <div><i class="fa fa-spinner fa-spin prode-mensajes"></i> {{:msg}}</div>
            {{/if}}
        </script>

        <script src="js/lib/jquery-1.10.2.min.js"></script>
        <script src="js/lib/jquery-ui-1.10.4.custom.js"></script>
        <script src="js/lib/bootstrap.min.js"></script>
        <script src="js/lib/jsviews.min.js"></script>
        <script src="js/lib/back-to-top.js"></script>
        <script src="js/lib/jquery.bootstrap.wizard.js"></script>
        <script src="js/lib/bootstrap-editable.js"></script>
        <script src="js/lib/jquery.plugin.js"></script> 
        <script src="js/lib/jquery.countdown.js"></script>
        <script src="js/lib/jquery-dateFormat.js"></script>
        <script src="js/app/prode.js"></script>
        <script src="js/app/ui/ui.js"></script>
        <script src="js/app/service/service.js"></script>
        <script src="js/app/service/grupo/grupo.js"></script>
        <tiles:importAttribute name="js" scope="page" />
        <c:forEach var="item_js" items="${js}">
            <script src="<tiles:insertAttribute value="${item_js}" />"></script>
        </c:forEach>
    </body>
</html>
