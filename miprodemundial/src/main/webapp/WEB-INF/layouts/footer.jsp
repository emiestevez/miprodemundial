<div id="footer">
    <div class="container">
        <div class="row-fluid pull-center">
            <!--@todo: replace with company social media links-->
            <div class="social-media">
                <a title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                <a title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
                <a title="Google+" href="#"><i class="fa fa-google-plus"></i></a>
            </div> 
            <!--@todo: replace with company copyright details-->
            <section class="copyright"><a href="/">Mi prode mundial</a> &copy; 2014    All rights reserved.</section>
        </div>
    </div>
</div>