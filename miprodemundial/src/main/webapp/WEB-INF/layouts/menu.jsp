<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="prode-menu">
    <div class="container">
        <sec:authorize access="isAuthenticated()">
            <div class="pull-right prode-link-menu">
                <a href="torneo.html">Torneo</a>
                <a href="mispronosticos.html">Mis pronosticos</a>
                <a href="reglas.html">Reglas</a>
            </div> 
        </sec:authorize>
        <sec:authorize access="!isAuthenticated()">
            <div class="pull-right prode-link-menu">
                <a href="registrar.html">Registrar</a>
                <a href="login.html">Login</a>
            </div>
        </sec:authorize>
    </div>
</div>
