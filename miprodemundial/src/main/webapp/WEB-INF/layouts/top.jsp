<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="prode-top">
    <div class="container">
        <sec:authorize access="isAuthenticated()">
            <div class="pull-right prode-link-menu">
                <a href="perfil.html">
                    <sec:authentication property="principal.username"/>
                </a>
                <a href="<c:url value="j_spring_security_logout" />">Logout</a>
            </div> 
        </sec:authorize>
    </div>
</div>
