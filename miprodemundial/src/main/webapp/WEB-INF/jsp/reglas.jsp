<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="prode-pagina-contenido">
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left prode-titulo-pagina">Reglas</h1>
        </div><!--/container-->
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-1">		
            <p class="prode-pregunta">�C&oacute;mo sumo puntos?</p>
            <p>Los puntos se computan de la siguiente manera:</p>
            <p><i class="fa fa-star"></i> 2 puntos por acertar el resultado del partido(L-E-V)</p>
            <p><i class="fa fa-star"></i> 1 punto  por acertar la cantidad de goles de un equipo</p>
            <p><i class="fa fa-star"></i> 1 punto  por diferencia de gol del partido</p>

            <p>Ejemplo</p>
            
            <p>El partido finaliza con el siguiente resultado: Argentina 2 -  Brasil 1 </p>
            <table class="table">
                <thead>
                    <tr>
                        <th>Pronostico</th>
                        <th>Cantidad de puntos que suma</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>2 - 1</td>
                        <td>5</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>3 - 2</td>
                        <td>3</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>2 - 0</td>
                        <td>3</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>3 - 0</td>
                        <td>2</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>1 - 1</td>
                        <td>1</td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>0 - 0</td>
                        <td>0</td>
                        <td>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br>
            <p class="prode-pregunta">�Hasta cuando puedo modificar el pronostico de un partido?</p>
            <p>Se podr&aacute; modificar los pronosticos de un partido hasta el d&iacute;a anterior al partido.</p>

            <br>
            <p class="prode-pregunta">�Cuando van a estar habilitado octavos, cuartos, etc..?</p>
            <p>Los partidos de octavos, cuartos, semifinal y final seran habilitados una vez que esten conformados los cuadros</p>
            
            <br>
            <p class="prode-pregunta">�Hasta cuando se puede agregar amigos al torneo?</p>
            <p>Se puede agregar en cualquier instancia del torneo. Si el mundial ya empezo, todos los partidos sobre el cual no pudo realizar un pronostico le ser&aacute; otorgado 0 puntos por partido.</p>
        </div>
    </div>
</div>
