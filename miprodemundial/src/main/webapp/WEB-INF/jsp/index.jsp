
<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" id="ha-header">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand logo" href="index.html">Project name</a> </div>
        <div class="navbar-collapse collapse ">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="registrar.html">Registrate</a></li>
                <li><a href="login.html">Login</a></li>
            </ul>
        </div>
        <!--/.nav-collapse --> 
    </div>
</div>
<!-- End Fixed navbar --> 

<!-- Full Page Image Header Area -->
<div id="top" class="header">
    <div class="flexslider">
        <ul class="slides">
            <li>
                <img src="img/slider/argentina.jpg"  alt="slider" />
                <p class="flex-caption">Hace tus pronosticos</p>
            </li>
            <li>
                <img src="img/slider/portugal.jpg" alt="slider" />
                <p class="flex-caption">juga con tus amigos</p>
            </li>
            <li>
                <img src="img/slider/espana.jpg"  alt="slider" />
                <p class="flex-caption">vivi el mundial de brasil</p>
            </li>
        </ul>
    </div>
</div>
<!-- /Full Page Image Header Area --> 

<!-- Services -->
<div id="services" class="services ha-waypoint"  data-animate-down="ha-header-small" data-animate-up="ha-header-large">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-sm-12">
                <div class=" container TitleSection">
                    <header class="page-head">
                        <h1>Mi prode mundial<small></small></h1>
                    </header>
                </div>
                <div class="row">
                    <div class="ser_wrap_3">
                        <div class="col-xs-12 col-md-4 col-sm-6">
                            <ul>
                                <li>
                                    <div class="icon_ser"><i class="fa fa-money"></i></div>
                                    <div class="wrap">
                                        <h3>Gratuito</h3>
                                        <p>Sin costo de inscripci&oacute;n.</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon_ser"><i class="fa fa-twitter"></i></div>
                                    <div class="wrap">
                                        <h3>Twitter</h3>
                                        <p>Segui los tweets del torneo.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-md-4 col-sm-6">
                            <ul>
                                <li>
                                    <div class="icon_ser"><i class="fa fa-users"></i></div>
                                    <div class="wrap">
                                        <h3>Amigos</h3>
                                        <p>Podes incluir a todos tus amigos</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon_ser"><i class="fa fa-list-ol"></i></div>
                                    <div class="wrap">
                                        <h3>Ranking</h3>
                                        <p>Ranking de posiciones</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-md-4 col-sm-6">
                            <ul>
                                <li>
                                    <div class="icon_ser"><i class="fa fa-trophy"></i></div>
                                    <div class="wrap">
                                        <h3>Brasil 2014</h3>
                                        <p>Hace tus pronosticos y gana!</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="icon_ser"><i class="fa fa-calendar"></i></div>
                                    <div class="wrap">
                                        <h3>Partidos</h3>
                                        <p>Calendario de todos los partidos</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<div id="footer">
    <div class="prode-footer-header">
    </div>
    <div class="container">
        <div class="row-fluid pull-center">
            <!--@todo: replace with company social media links-->
            <div class="social-media">
                <a title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                <a title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
                <a title="Google+" href="#"><i class="fa fa-google-plus"></i></a>
            </div> 
            <!--@todo: replace with company copyright details-->
            <section class="copyright"><a href="/">Mi prode mundial</a> &copy; 2014    All rights reserved.</section>
        </div>
    </div>
</div>





