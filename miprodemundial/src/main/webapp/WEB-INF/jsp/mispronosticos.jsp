<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="prode-pagina-contenido">

    <div class="container">
        <div class="breadcrumbs">
            <div class="pull-left prode-titulo-pagina">
                Mis pronosticos
            </div>
        </div><!--/container-->
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3" id="pronosticoMensajes"></div>
        <div id="spinnerPantallaMisPronosticos" class="prode-spinner"></div>
    </div>

    <div class="container">
        <div>
            <div class="col-md-2 pull-right">
                <button type="button" id="guardarPronostico" class="btn-u btn-lg btn-block prode-pronostico-boton-guardar">GUARDAR</button>   
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="mispronosticos"></div>
            </div>
        </div>
    </div>
</div>

<script id="prodeTemplate" type="text/x-jsrender">
    {{for mundial.faseDeGrupos tmpl="#misPronosticosTemplate"/}}
</script>    

<script id="misPronosticosTemplate" type="text/x-jsrender">
    <div class="col-md-6 prode-pronosticos-grupo-nombre prode-js-pronosticos-grupo">
    <div class="prode-pronosticos-grupo">
    <label>{{>nombre}}</label>
    </div>
    <div>
    <table class="table table-hover">
    <thead class="prode-pronostico-table-thead">
    <tr>
    <td>Fecha</td>
    <td>Tu pronostico</td>
    <td>Como termino</td>
    <td>Puntos</td>
    </tr>
    </thead>
    <tbody class="prode-pronostico-table-tbody">
    {{for pronosticos tmpl="#partidoTemplate"/}}
    </tbody>
    </table>
    </div>
    </div>
</script>    

<script id="partidoTemplate" type="text/x-jsrender">
    <tr>
    <td>{{>~diaDelPartido(partido.fecha)}}</td>
    <td ><img class="prode-pronostico-bandera" src={{>partido.local.bandera}}> 
    {{>partido.local.nombre}}
    <input type="text" maxlength="2" size="2" data-link="golesLocal">  -  <input type="text" maxlength="2" size="2" data-link="golesVisitante">
    <img class="prode-pronostico-bandera" src={{>partido.visitante.bandera}}> 
    {{>partido.visitante.nombre}}</td>
    {{if partido.partidoTerminado === false}}
        <td>-</td>
    {{else}}
    <td>{{>partido.golesLocal}} - {{>partido.golesVisitante}}</td>
    {{/if}}
    <td>0</td>
    </tr>
</script> 

<script id="mensajeAlertaTemplate" type="text/x-jsrender">
    {^{if msg}}
    <div class="alert alert-block center alert-info">{{:msg}}</div>
    {{/if}}
</script>

<script id="spinnerTemplate" type="text/x-jsrender">
    {^{if msg}}
    <div><i class="fa fa-spinner fa-spin"></i> {{:msg}}</div>
    {{/if}}
</script>
