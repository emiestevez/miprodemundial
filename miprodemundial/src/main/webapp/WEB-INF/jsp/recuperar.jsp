<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="prode-pagina-contenido">

    <div class="breadcrumbs margin-bottom-40">
        <div class="container">
            <h1 class="pull-left prode-titulo-pagina">�Olvidaste tu contrase�a?</h1>
        </div><!--/container-->
    </div>

    <div class="container">		
        <div class="row">
            <div class="col-md-5 col-md-offset-3" id="mensajeRecuperar"></div>
            <div id="spinnerPantallaRecuperar" class="col-md-6 col-md-offset-3 prode-spinner"></div>
            <div class="col-md-5 col-md-offset-3">
                <p>
                    Ingresa tu direcci&oacute;n de correo electr&oacute;nico
                    <br>
                    <span class="prode-registrar-span">
                        <input type="email" id="email" aria-invalid="false" aria-required="true" class="prode-registrar-input" size="40" value="" name="your-name">
                    </span> 
                </p>
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" id="recuperar" class="btn-u btn-lg btn-block prode-boton-login">Enviar</button>                        
                    </div>
                </div>
            </div>

        </div><!--/row-->
    </div>
</div>
