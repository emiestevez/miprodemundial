<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="breadcrumbs">
    <div class="container">
        <div class="pull-left prode-titulo-pagina">
            Ranking
        </div>
        
        <div id="pronosticoMensajes">
        </div>
    </div><!--/container-->
</div>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div id="rankingTorneo"></div>
        </div>
        <div class="col-md-6">
            <h3 class="prode-ranking-h3">Resultados</h3>
        </div>
    </div>
</div>

<script id="rankingTorneoTemplate" type="text/x-jsrender">
    <h3 class="prode-ranking-h3">{{>nombre}}</h3>
    {{for prodes tmpl="#rankingMiembroTemplate"/}}
</script>

<script id="rankingMiembroTemplate" type="text/x-jsrender">
    {{:index}} {{>usuario.nombre}} - {{>puntos}} ptos
</script>



<script id="mensajeAlertaTemplate" type="text/x-jsrender">
    {^{if msg}}
    <div class="alert alert-block center alert-info">{{:msg}}</div>
    {{/if}}
</script>
