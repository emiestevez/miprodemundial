<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="prode-pagina-contenido">
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left prode-titulo-pagina">Mi perfil</h1>
        </div>
    </div>

    <div class="row">
        <div id="spinner" class="prode-spinner"></div>
    </div>

    <div class="row">
        <div class="container">
            <div class="col-md-12">
                <button type="button" class="btn-lg prode-pronostico-boton-guardar prode-js-perfil-editar pull-right">Editar</button>   
            </div>
            <div class="col-md-12">
                <button type="button" class="btn-lg prode-pronostico-boton-guardar prode-js-perfil-guardar pull-right hide">Guardar</button>   
                <button type="button" class="btn-lg prode-pronostico-boton-guardar prode-js-perfil-cancelar pull-right hide">Cancelar</button>   
            </div>
        </div>
    </div>

    <div class="row">
        <div id="perfil" class="col-md-offset-1"></div>
    </div>
</div>

<script id="perfilTemplate" type="text/x-jsrender">

    <form role="form" class="form-horizontal prode-js-perfil">
    <div class="form-group">
    <label class="col-sm-2 control-label">Nombre</label>
    <div class="col-sm-10">
    <span class="prode-perfil-dato prode-js-perfil-nombre" data-link="nombre"></span>
    <input type="text" class="form-control prode-js-perfil-editar-nombre hide" data-link="nombre"/>
    </div>    
    </div>
    <div class="form-group">
    <label class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
    <span class="prode-perfil-dato" data-link="email"></span>
    <input type="text" class="form-control prode-js-perfil-editar-email hide" data-link="email"/>
    </div>    
    </div>

    <div class="form-group">
    <label class="col-sm-2 control-label">Torneo</label>
    <div class="col-sm-10">
    <span class="prode-perfil-dato" data-link="email"></span>
    <input type="text" class="form-control prode-js-perfil-editar-email hide" data-link="email"/>
    </div>    
    </div>

    </form>
</script>

<script id="mensajeAlertaTemplate" type="text/x-jsrender">
    {^{if msg}}
    <div class="alert alert-block center alert-info">{{:msg}}</div>
    {{/if}}
</script>

<script id="spinnerTemplate" type="text/x-jsrender">
    {^{if msg}}
    <div><i class="fa fa-spinner fa-spin"></i> {{:msg}}</div>
    {{/if}}
</script>
