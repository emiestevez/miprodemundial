<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="prode-pagina-contenido">

    <div class="breadcrumbs margin-bottom-40">
        <div class="container">
            <h1 class="pull-left prode-titulo-pagina">Registraci&oacute;n</h1>
        </div><!--/container-->
    </div>

    <div class="container">		
        <div class="row">
            <div class="col-md-6 col-md-offset-3" id="mensajeLogin"></div>
            <div id="spinnerPantallaRegistrar" class="col-md-6 col-md-offset-3 prode-spinner"></div>
            <div class="col-md-5 col-md-offset-3">
                <p>
                    Tu nombre
                    <br>
                    <span class="prode-registrar-span">
                        <input type="text" id="nombre" aria-invalid="false" aria-required="true" class="prode-registrar-input" size="40" value="" name="your-name">
                    </span> 
                </p>
                <p>
                    Tu email
                    <br>
                    <span class="prode-registrar-span">
                        <input type="email" id="email" aria-invalid="false" aria-required="true" class="prode-registrar-input" size="40" value="" name="your-name">
                    </span> 
                </p>
                <p>
                    Tu password
                    <br>
                    <span class="prode-registrar-span">
                        <input type="password" id="password" aria-invalid="false" aria-required="true" class="prode-registrar-input" size="40" value="">
                    </span> 
                </p>
                <p>
                    Repeti tu Password
                    <br>
                    <span class="prode-registrar-span">
                        <input type="password" id="passwordReply" aria-invalid="false" aria-required="true" class="prode-registrar-input" size="40" value="">
                    </span> 
                </p>
                
                <p>
                    Nombre del torneo
                    <br>
                    <span class="prode-registrar-span">
                        <input type="text" id="nombreTorneo" aria-invalid="false" aria-required="true" class="prode-registrar-input" size="40" value="">
                    </span> 
                </p>

                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" id="registro" class="btn-u btn-lg btn-block prode-boton-login">REGISTRAR</button>                        
                    </div>
                </div>
            </div>

        </div><!--/row-->
    </div>
</div>
