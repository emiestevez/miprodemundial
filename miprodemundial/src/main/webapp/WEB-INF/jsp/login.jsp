<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div class="container">
    <div class="prode-pagina-contenido">		

        <div class="breadcrumbs">
            <div class="container">
                <div class="pull-left prode-titulo-pagina">
                    Login
                </div>
            </div><!--/container-->
        </div>

        
        <div class="col-md-12">
            <c:if test="${sessionScope.SPRING_SECURITY_LAST_EXCEPTION != null}" >
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    Usuario o contraseña incorrecto.  
                </div>
                <c:remove var="SPRING_SECURITY_LAST_EXCEPTION" scope="session" />
            </c:if>
            <div class="col-md-5 col-md-offset-3">
                <form method="POST" action="<c:url value="/j_spring_security_check"/>" class="form-horizontal">
                    <p>
                        Email
                        <span class="prode-registrar-span">
                            <input type="text" id="nombre" name="j_username" aria-invalid="false" aria-required="true" class="prode-registrar-input" size="40" value="" name="your-name">
                        </span> 
                    </p>

                    <p>
                        Password
                        <span class="prode-registrar-span">
                            <input type="password" id="password" name="j_password" aria-invalid="false" aria-required="true" class="prode-registrar-input" size="40" value="" name="your-name">
                        </span> 
                    </p>

                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn-u btn-lg btn-block prode-boton-login">LOGIN</button>                        
                        </div>
                    </div>

                    <hr>

                    <h4>¿Olvidaste tu contraseña?</h4>
                    <p>no te preocupes, <a href="recuperar.html" class="color-green">hace click ac&aacute;</a> para volver a generar tu contraseña.</p>
                </form>            
            </div>
        </div><!--/row-->
    </div>
</div>

<script id="mensajeAlertaTemplate" type="text/x-jsrender">
    {^{if msg}}
    <div class="alert alert-block center alert-info">{{:msg}}</div>
    {{/if}}
</script>
