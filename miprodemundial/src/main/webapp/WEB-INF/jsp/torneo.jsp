<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<div class="prode-pagina-contenido">
    <div class="container">
        <div class="breadcrumbs">
            <div class="container">
                <div class="pull-left prode-titulo-pagina">
                    Torneo
                </div>
                <div id="mensajePantallaTorneo"></div>
            </div><!--/container-->
        </div>

        <div class="row">
            <div id="spinnerPantallaMisPronosticos" class="prode-spinner"></div>
        </div>

        <div class="row">
            <div class="col-md-4 pull-right">
                <button type="button" id="editarTorneo" class="btn-lg prode-pronostico-boton-guardar">Editar</button>   
                <button type="button" id="guardarTorneo" class="btn-lg prode-pronostico-boton-guardar">Guardar</button>   
            </div>
        </div>
        <div class="row">
            <div class="container">
                <div class="col-md-10 col-md-offset-1">
                    <div id="nombreTorneo" class="prode-nombre-torneo">
                        <label>Nombre</label>
                        <a class="prode-js-nombre-torneo editable editable-disabled">Mundial Brasil 2014</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="container">
                <div class="prode-torneo-formulario-nuevo-amigo col-md-8 col-md-offset-2">
                    <sec:authorize access="hasRole('admin')">
                        <form class="form-inline" role="form">
                            <div class="form-group">
                                <label class="sr-only">Nombre</label>
                                <input type="text" class="form-control prode-js-nombre-amigo" placeholder="Nombre" tabindex="1">
                            </div>
                            <div class="form-group">
                                <label class="sr-only">Email</label>
                                <input type="email" class="form-control prode-js-email-amigo " placeholder="Email" size="40px"  tabindex="2">
                            </div>
                            <button type="button" class="btn pull-right" id="nuevoAmigo"  tabindex="3">
                                <i class="fa fa-user"></i> Agregar amigos
                            </button>
                        </form>
                    </sec:authorize>
                </div>

                <div id="listaDeAmigos" class="prode-torneo-listado-amigos col-md-6 col-md-offset-3">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Puntos</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="miembrosDelTorneo">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script id="mensajeTemplate" type="text/x-jsrender">
    {^{if msg}}
    <div class="alert alert-block center alert-info">{{:msg}}</div>
    {{/if}}
</script>

<script id="spinnerTemplate" type="text/x-jsrender">
    {^{if msg}}
    <div><i class="fa fa-spinner fa-spin"></i> {{:msg}}</div>
    {{/if}}
</script>

<script id="torneoTemplate" type="text/x-jsrender">
    {^{for}}
    <tr>
    <td>{{:usuario.nombre}}</td>
    <td>{{:usuario.email}}</td>
    <td>{{:puntos}}</td>
    <td><i class="fa fa-pencil"></i> <i class="fa fa-trash-o"></i></td>
    </tr>
    {{/for}}
</script>



