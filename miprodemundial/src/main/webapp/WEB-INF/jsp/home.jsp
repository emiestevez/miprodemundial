<div class="prode-pagina-contenido">
    <div class="breadcrumbs">
        <div class="container">
            <div class="pull-left prode-titulo-pagina">
                Torneo
            </div>
            <div id="mensajePantallaTorneo"></div>
        </div>
        <!--/container-->

    </div>

    <div class="row">
        <div id="spinnerPantallaMisPronosticos" class="prode-spinner"></div>
    </div>

    <div class="row">
        <div class="container">
            <div class="col-md-8">
                <div class="prode-home-ranking col-md-offset-3">
                </div>
                <div id="torneo" class="col-md-8 col-md-offset-2"></div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    Pr&oacute;ximos partidos
                </div>
                <div class="row">
                    <table>
                        <thead>
                        <tr>
                            <th>Hora</th>
                            <th>Partido</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="3">Jueves 12 de Junio</td>
                        </tr>
                        <tr>
                            <td>17:00</td>
                            <td>Brasil - Croacia</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3">Jueves 13 de Junio</td>
                        </tr>
                        <tr>
                            <td>13:00</td>
                            <td>Mexico - Camerun</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>16:00</td>
                            <td>EspaÃ±a - Holanda</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>19:00</td>
                            <td>Chile - Autralia</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <a class="twitter-timeline" href="https://twitter.com/search?q=%23miprodemundial"
                   data-widget-id="465200756073574400">Tweets sobre "#miprodemundial"</a>
            </div>
        </div>
    </div>
</div>


<script id="torneoTemplate" type="text/x-jsrender">
    <table class="table">
        <col width="5%">
        <col width="80%">
        <col width="15%">
        <tbody>
            {{for prodes tmpl="#miembrosDelTorneoTemplate"/}}
        </tbody>
    </table>


</script>

<script id="miembrosDelTorneoTemplate" type="text/x-jsrender">
    <tr>
    <td></td>
    <td style="font-family: 'brasil2014';text-align: center;font-size: 30px; color:#50B777;">Santiago Estevez</td>
    <td> 129 pts</td>
    </tr>
    <tr>
    <td>{{:#index+1}}</td>
    <td>{{>usuario.nombre}}</td>
    <td>{{>puntos}}</td>
    </tr>


</script>

<script id="proximosPartidosTemplate" type="text/x-jsrender">
    <div class="prode-home-partidos">
    <div class="row">
    <div class="col-md-3" style="font-size: 40px; text-align:center;margin-right:-20px">
    <label>{{>~dia(fecha)}}</label>
    </div>
    <div class="col-md-8">
    <div>{{>~diaDeLaSemana(fecha)}}</div>
    <div>{{>~mes(fecha)}} 19hs</div>
    <div>Rio de janeiro</div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-6 col-md-offset-1">
    <img class="prode-pronostico-bandera" src={{>local.bandera}}> - 
    <img class="prode-pronostico-bandera" src={{>visitante.bandera}}>
    </div>
    </div>
    </div>


</script>


<script>!function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
    if (!d.getElementById(id)) {
        js = d.createElement(s);
        js.id = id;
        js.src = p + "://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);
    }
}(document, "script", "twitter-wjs");</script>
